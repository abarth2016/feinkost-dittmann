<?php
defined('_JEXEC') or die;
/*Tabellendefinition der Einzelansicht */
class RezepteingabeTableRezept extends JTable{

    public function __construct(&$db){
        parent::__construct('recipes', 'id', $db);
    }
}

class RezepteingabeTableZutaten extends JTable{

    public function __construct(&$db){
        parent::__construct('ingredients', 'id', $db);
    }
}