<?php
defined('_JEXEC') or die;
?>
<form action="<?php echo JRoute::_('index.php?option=com_rezepteingabe&view=rezept&layout=edit&id='.(int) $this->recipe->id);?>"
    method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('bezeichnung');?></div>
                    <div class="controls"><?php echo html_entity_decode($this->form->getInput('bezeichnung'));?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('zubereitung');?></div>
                    <div class="controls"><?php echo html_entity_decode($this->form->getInput('zubereitung'));?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('kategorie');?></div>

                    <div class="controls"><?php echo $this->form->getInput('kategorie','',$this->recipe->kategorie[0]->kategorie_id);?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('uploadfile_smallest');?></div>
                    <div class="controls"><?php echo $this->form->getInput('uploadfile_smallest');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('uploadfile_small');?></div>
                    <div class="controls"><?php echo $this->form->getInput('uploadfile_small');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('uploadfile');?></div>
                    <div class="controls"><?php echo $this->form->getInput('uploadfile');?></div>
                </div>
                <div class="control-group">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <th>Menge</th>
                        <th>Mengeneinheit</th>
                        <th>Bezeichnung</th>
                        <th>Artikel-Nr.</th>
                    </tr>
                <?php for ($i = 0; $i<=21; $i++){?>
                    <tr>
                    <td><input name="jform[ingredient_id][]" id="jform_quantity" value="<?php echo $this->recipe->ingredients[$i]->id;?>" class="inputbox" size="3" type="hidden">
                    <input type="text" name="jform[quantity][]" id="jform_quantity" value="<?php echo $this->recipe->ingredients[$i]->quantity;?>" class="inputbox" size="3"></td>
                    <td><?php echo html_entity_decode($this->form->getInput('mesureunit','',$this->recipe->ingredients[$i]->measuringunit));?></td>
                    <td><input type="text" name="jform[ingredient][]" id="jform_ingredient" value="<?php echo $this->recipe->ingredients[$i]->bezeichnung;?>" class="inputbox" size="3"></td>
                    <td><input type="text" name="jform[artikel_nr][]" id="jform_artikel_nr" value="<?php echo $this->recipe->ingredients[$i]->artikel_nr;?>" class="inputbox" size="5"> </td>
                    </tr>
                <?php } ?>
                </table>
                </div>
                <input type="hidden" name="task" value="">
                <?php echo JHtml::_('form.token');?>
            </fieldset>
            Looks for images named: <?php echo $this->recipe->filename;?>
            <?php
            $images[]          =  './../images' .DIRECTORY_SEPARATOR.'rezeptbilder'.DIRECTORY_SEPARATOR.'220x240'.DIRECTORY_SEPARATOR. $this->recipe->filename;
            $images[]          =  './../images' .DIRECTORY_SEPARATOR.'rezeptbilder'.DIRECTORY_SEPARATOR.'618x390'.DIRECTORY_SEPARATOR. $this->recipe->filename;
            $images[]          =  './../images' .DIRECTORY_SEPARATOR.'rezeptbilder'.DIRECTORY_SEPARATOR.'1430x425'.DIRECTORY_SEPARATOR. $this->recipe->filename;

            echo "<ul style='list-style-type: none'>";
            foreach($images as $image){
                if(file_exists($image)){
                    
                    echo "<li style='clear:both'>".$image."</li>";
                    echo '<li style="clear:both;float:left"><img style="margin-bottom:20px;" src="'.$image.'" style="max-width:500px" alt="'.$image.'"></li>';
                }
            }
            echo "</ul>";
            ?>
        </div>
    </div>
</form>
