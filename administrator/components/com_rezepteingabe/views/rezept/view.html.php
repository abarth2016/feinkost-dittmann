<?php
defined('_JEXEC') or die;
/* View-Variante für das einzelne "Rezept"-Formular zur Neuanlage und Beaerbeitung */
class RezeptEingabeViewRezept extends JViewLegacy
{
    protected $recipe;
    protected $form;
    
    public function display( $tpl=null ){

        $this->recipe = $this->get('Item');

        // Get the form
        $this->form = $this->get('Form');
        JFactory::getApplication()->input->set('hidemainmenu', true);
        JToolbarHelper::title(Jtext::_('COM_REZEPTEINGABE_MANAGER_REZEPT'),'');

        //Button hinzufügen
        JToolbarHelper::save('rezept.save');
        //JFactory::getApplication()->redirect('index.php?option=com_rezepteingabe&view=rezepte');


        if(empty($this->recipe->id))
        {
            JToolbarHelper::cancel('rezept.cancel', 'JTOOLBAR_CANCEL');
        }
        else
        {
            JToolbarHelper::cancel('rezept.cancel', 'JTOOLBAR_CLOSE');
        }

        parent::display($tpl);
    }
 /*   public function delete( $tpl=null ){
        
        parent::display($tpl);
    }*/

}