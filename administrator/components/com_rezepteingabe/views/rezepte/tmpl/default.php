<?php
defined('_JEXEC') or die;
?>
<form action="<?php echo JRoute::_('index.php?option=com_rezepteingabe&view=rezepte')?>" method="post" name="adminForm" id="adminForm">
    <div id="j-main-container">
        <div class="clearfix"></div>
        <table class="table table-striped" id="rezepteList">
            <thead>
              <tr>
                  <th class="nowrap center">
                      <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL');?>" onClick="Joomla.checkAll(this)">
                  </th>
                  <th>
                      <?php echo JText::_('COM_REZEPTEINGABE_COLUMN_HEADER_ID');?>
                  </th>
                  <th>
                      <?php echo JText::_('COM_REZEPTEINGABE_COLUMN_HEADER_BEZEICHNUNG');?>
                  </th>
                  <th>
                      <?php echo JText::_('COM_REZEPTEINGABE_COLUMN_HEADER_ZUBEREITUNG');?>
                  </th>
                  <th>
                      <?php echo JText::_('COM_REZEPTEINGABE_COLUMN_HEADER_KATEGORIE');?>
                  </th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($this->recipes as $i=>$recipe): ?>
            <tr class="row<?php echo $i % 2; ?>">
              <td class="nowrap center">
              <?php  echo JHtml::_('grid.id', $i, $recipe->id);?>
              </td>
                <td class="nowrap center">
                    <?php  echo  $recipe->id;?>
                </td>
              <td class="has-context">
                  <a href="<?php echo JRoute::_('index.php?option=com_rezepteingabe&task=rezept.edit&id='.(int) $recipe->id);?>">
                  <?php echo $this->escape(html_entity_decode($recipe->bezeichnung));?>
                  </a>
              </td>
            <td class="small">
                <?php echo substr(html_entity_decode($this->escape($recipe->zubereitung)),0,200);?>
            </td>
                <td>
                    <?php echo $this->escape($recipe->kategorie_id);?>
                </td>
            </tr>
            <?php endforeach;?>
            </tbody>
        </table>

        <input type="hidden" name="task" value="">
        <input type="hidden" name="boxchecked" value="0">
        <?php echo JHtml::_('form.token');?>
        <?php echo $this->pagination->getListFooter(); ?>

    </div>
</form>
