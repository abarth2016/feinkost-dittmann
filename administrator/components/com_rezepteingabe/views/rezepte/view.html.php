<?php
defined('_JEXEC') or die;
/* view.html.php erhält die Daten aus dem Model, bereitet sie ggf. auf und reicht sie an das Template default.php weiter */
class RezepteingabeViewRezepte extends JViewLegacy{
    protected $recipes;
    
    public function display($tpl = null)
    {
        //holt sich alle Daten aus dem Model und stellt sie dem Template in der Variablen $recipes zur Verfügung
        $this->recipes = $this->get('Items');
        $this->pagination = $this->get('Pagination');
        
        $bar = JToolBar::getInstance('toolbar');

        JToolbarHelper::title(JText::_('COM_REZEPTEINGABE_MANAGER_REZEPTE'));
        JToolbarHelper::addNew('rezept.add');
        JToolbarHelper::editList('rezept.edit');
        JToolbarHelper::deleteList(JText::_('COM_REZEPTEINGABE_DELETE_CONFIRMATION'), 'rezepte.delete', 'JTOOBAR_DELETE');

        parent::display($tpl);
    }

}