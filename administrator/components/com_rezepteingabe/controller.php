<?php
defined('_JEXEC') or die;

/* Komponenten-Controller zur Initalisierung der Anzeige */
class RezepteingabeController extends JControllerLegacy
{
    //wenn default_view nicht gesetzt wurde und kein View-Angabe mit übergeben wurde wird versucht den Komponentennamen als Ansicht aufzurufen
    protected $default_view = "rezepte";
    public function display($cachable = false, $urlparams = false)
    {
        //vorereiteter Code hier können gezielte Anweisungen audgeführt werden
        //wenn eine bestimmte LAyout-View-Kombination zur Verfügung gestellt wird
        $view = $this->input->get('view', 'rezepte');
        $layout = $this->input->get('layout', 'default');
        $id = $this->input->getInt('id');
        $task = $this->input->get('task');

        if ($view == 'rezept' && $layout == 'edit' && !$this->checkEditId('com_rezepteingabe.edit.rezept', $id))
		{
            // Somehow the person just went to the form - we don't allow that.
            $this->setMessage($this->getError(), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_rezepteingabe&view=rezept&id='.$id, false));

            return false;
        }
        if ($view == 'rezepts')
        {
            // Somehow the person just went to the form - we don't allow that.
            $this->setRedirect(JRoute::_('index.php?option=com_rezepteingabe&view=rezepte', false));

            return false;
        }

        parent::display();

        return $this;
    }
    /**
     * Save the incoming data and then return to the Browse task
     *
     * @return  bool
     */

}