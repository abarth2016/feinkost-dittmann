<?php
defined('_JEXEC') or die;
/* Standard-Model für die Datenbankabfrage aller Datenbankeinträge der recipes-Tabelle */
class RezepteingabeModelRezepte extends JModelList
{
    protected function getListQuery()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(array('recipes.id','bezeichnung','zubereitung','kategorie_id')));
        $query->from($db->quoteName('recipes'));
        $query->join('LEFT', 'rezepte_kategorie on rezepte_kategorie.rezepte_id=recipes.id');

        $query->order('recipes.id DESC');
        return $query;
    }
}