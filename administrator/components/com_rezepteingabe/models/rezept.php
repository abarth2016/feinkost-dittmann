<?php
defined('_JEXEC') or die;
/* This model shows and edits one recipe */
class RezeptEingabeModelRezept extends JModelAdmin
{
    protected $imageDestinations = [
        'small'     => "../images/rezeptbilder/220x240/",
        'normal'    => "../images/rezeptbilder/618x390/",
        'wide'      => "../images/rezeptbilder/1430x425/"
    ];
    
    public function getTable($type = 'Rezept', $prefix = 'RezepteingabeTable', $config = array())
    {
        return JTable::getInstance($type,$prefix,$config);
    }

    public function getIngredientsTable($type = 'Zutaten', $prefix = 'RezepteingabeTable', $config = array()){
        return JTable::getInstance($type,$prefix,$config);
    }
    
    public function getForm($data = array(), $loadData = true)
    {
        $app = JFactory::getApplication();
        $form = $this->loadForm('com_rezepteingabe.rezept','rezept', array('control'=>'jform','load_data' =>$loadData));
        if(empty($form))
        {
           return false;
        }
        return $form;
    }
    public function delete(&$pks){
        //delete the recipe(s)
        parent::delete($pks);
        
        //delete entries from ingredients table for the recipe
        foreach( $pks as $pk) {
            $db = $this->getDbo();
            $query_deleteIngredient = $db->getQuery(true);
            $query_deleteIngredient->delete('ingredients');
            $query_deleteIngredient->where('recipe_id=' . $pk);
            $db->setQuery($query_deleteIngredient);
            $db->execute();

            //delete entry from rezepte_kategorie for this recipe
            $db = $this->getDbo();
            $query_recCat = $db->getQuery(true);
            $query_recCat->delete('rezepte_kategorie');
            $query_recCat->where('rezepte_id=' . $pk);
            $db->setQuery($query_recCat);
            $db->execute();
        }
    }

    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState('com_rezepteingabe.edit.rezept.data', array());

        if(empty($data)){
            $data = $this->getItem();
            if(!empty($data->id)) {
                //Zutaten hinzufügen
                $db1 = $this->getDbo();
                $query = $db1->getQuery(true);
                $query->select($db1->quoteName(array('id','bezeichnung', 'quantity', 'measuringunit', 'artikel_nr')));
                $query->from($db1->quoteName('ingredients'));
                $query->where($db1->quoteName('recipe_id') . "=" . $data->id);
                $query->order('id ASC');
                $db1->setQuery($query);
                $data->ingredients = $db1->loadObjectList();
            }
        }
        return $data;
    }

    public function getItem($pk = null)
    {
        $result = parent::getItem($pk);
        
        //get row for recipe category also
        if(!empty($result->id)) {
            $db1 = $this->getDbo();
            $query = $db1->getQuery(true);
            $query->select($db1->quoteName('kategorie_id'));
            $query->from($db1->quoteName('rezepte_kategorie'));
            $query->where($db1->quoteName('rezepte_id') . "=" . $result->id);
            $db1->setQuery($query);
            $result->kategorie = $db1->loadObjectList();
        }
        //get recipe ingredients also
        if(!empty($result->id)) {
            $db1 = $this->getDbo();
            $query = $db1->getQuery(true);
            $query->select($db1->quoteName(array('id','bezeichnung', 'quantity', 'measuringunit', 'artikel_nr')));
            $query->from($db1->quoteName('ingredients'));
            $query->where($db1->quoteName('recipe_id') . "=" . $result->id);
            $query->order('id ASC');
            $db1->setQuery($query);
            $result->ingredients = $db1->loadObjectList();
        }
        if($result->bezeichnung){
            $createdFileName = $this->createSpeakingFilename($result->bezeichnung,$result->id);
            $result->filename = $createdFileName;
        }
        return $result;
    }


    public function saveIngredients($data){

        $dispatcher = JEventDispatcher::getInstance();
        $table      = $this->getIngredientsTable();

        $key = $table->getKeyName();
        $pk = (!empty($data[$key])) ? $data[$key] : (int) $this->getState($this->getName() . '.id');
        $isNew = true;

        // Include the plugins for the save events.
        JPluginHelper::importPlugin($this->events_map['save']);

        // Allow an exception to be thrown.
        try
        {
            // Load the row if saving an existing record.
            if ($pk > 0)
            {
                $table->load($pk);
                $isNew = false;
            }

            // Bind the data.
            if (!$table->bind($data))
            {
                $this->setError($table->getError());

                return false;
            }

            // Prepare the row for saving
            $this->prepareTable($table);

            // Check the data.
            if (!$table->check())
            {
                $this->setError($table->getError());

                return false;
            }


            // Store the data.
            if (!$table->store())
            {
                $this->setError($table->getError());
            }
        }
        catch (Exception $e)
        {
            $this->setError($e->getMessage());

            return false;
        }
        if (isset($table->$key))
        {
            $this->setState($this->getName() . '.id', $table->$key);

        }

        $this->setState($this->getName() . '.new', $isNew);
        return true;
    }
    protected function saveImages($data, $recipeId){
        //var_dump($_FILES['jform']['tmp_name']['uploadfile_smallest']);die();
        $createdFileName = $this->createSpeakingFilename(html_entity_decode($data['bezeichnung']),$recipeId);
        $destination_smallest =  "../images/rezeptbilder/220x240/" . $createdFileName;
        $destination_small    =  "../images/rezeptbilder/618x390/" . $createdFileName;
        $destination          =  "../images/rezeptbilder/1430x425/" . $createdFileName;


        //1430x425
        if (!move_uploaded_file(
            $_FILES['jform']['tmp_name']['uploadfile'],
            $destination)
        ){
            $this->setError("uploaddatei_smallest could not be coppied");

        }
        //220x240
        if (!move_uploaded_file(
            $_FILES['jform']['tmp_name']['uploadfile_smallest'],
            $destination_smallest)
        ){
            $this->setError("uploaddatei_smallest could not be coppied");

        }
        //618x390
        if (!move_uploaded_file(
            $_FILES['jform']['tmp_name']['uploadfile_small'],
            $destination_small)
        ){
            $this->setError("uploaddatei_small could not be coppied");
        }

    }
    protected function createSpeakingFilename(  $recipeHeadline, $recipeId, $fileextension = "jpg"){
        mb_internal_encoding("UTF-8");        
        $string = mb_strtolower($recipeHeadline);
        $string = preg_replace('/\x{00e4}/u', "ae", $string);
        $string = preg_replace('/\x{00fc}/u', "ue", $string);
        $string = preg_replace('/\x{00f6}/u', "oe", $string);
        $string = preg_replace('/\x{00df}/u', "ss", $string);
        $string = preg_replace('/\r\n/', "_", $string);
        $string = preg_replace('/\n/', "_", $string);
        $string = preg_replace('/\r/', "_", $string);
        $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
        $string = preg_replace("/-/", "_", $string);
        $string = preg_replace('/\x{0020}/u', "_", $string);
        $createdFileName = preg_replace("/[^a-z0-9_]/","",$string);
        //echo $createdFileName."\n";

        $destination = $createdFileName."_" . str_pad($recipeId, 3, 0) . "." . $fileextension;
        return $destination;
    }
    public function save($data){
        $dispatcher = JEventDispatcher::getInstance();
        $table      = $this->getTable();
        $context    = $this->option . '.' . $this->name;

        $key = $table->getKeyName();
        $pk = (!empty($data[$key])) ? $data[$key] : (int) $this->getState($this->getName() . '.id');
        $isNew = true;


        // Include the plugins for the save events.
        JPluginHelper::importPlugin($this->events_map['save']);

        // Allow an exception to be thrown.
        try
        {
            // Load the row if saving an existing record.
            if ($pk > 0)
            {
                $table->load($pk);
                $oldBezeichnung = $table->get('bezeichnung');

                $isNew = false;
            }
            // Bind the data.
            if (!$table->bind($data))
            {
                $this->setError($table->getError());

                return false;
            }

            // Prepare the row for saving
            $this->prepareTable($table);

            // Check the data.
            if (!$table->check())
            {
                $this->setError($table->getError());

                return false;
            }

            // Trigger the before save event.
            $result = $dispatcher->trigger($this->event_before_save, array($context, $table, $isNew));

            if (in_array(false, $result, true))
            {
                $this->setError($table->getError());

                return false;
            }

            if (($oldBezeichnung !== $data['bezeichnung']) && $oldBezeichnung!="")
            {
                //if title has changed images need to be renamed
                $oldFileName = $this->createSpeakingFilename($oldBezeichnung, $data['id']);
                $newFileName = $this->createSpeakingFilename($data['bezeichnung'], $data['id']);


                foreach($this->imageDestinations as $destionation){
                    if(file_exists($destionation.$oldFileName)){
                        if(!rename($destionation.$oldFileName, $destionation.$newFileName)){
                            $this->setError("Imagename could not be changed");
                        }
                    }
                }
            }
            // Store the data.
            if (!$table->store())
            {
                $this->setError($table->getError());

                return false;
            }else {
                //new row will be added or existing row will be updated
                $insertedId = $table->$key;

                $this->saveRezeptKategorie($data, $table->$key);

                try {
                    $i = 0;
                    foreach ($data['ingredient'] as $ingredient) {
                        if ($data['ingredient'][$i] == "" && $data['ingredient_id'][$i] !="") {
                            $db = $this->getDbo();
                            $query_deleteIngredient = $db->getQuery(true);
                            $query_deleteIngredient->delete('ingredients');
                            $query_deleteIngredient->where('id=' . $data['ingredient_id'][$i]);
                            $db->setQuery($query_deleteIngredient);
                            $stack[] = (string)$query_deleteIngredient;

                            $db->execute();
                        }
                        if ($data['ingredient'][$i] !="") {
                            $ingredientResult = false;
                            if ($data['ingredient_id'][$i] != ""){
                                $db = $this->getDbo();
                                $query_selectIngredient = $db->getQuery(true);
                                $query_selectIngredient->select('bezeichnung', 'quantity', 'measuringunit', 'recipe_id', 'artikel_nr');
                                $query_selectIngredient->from('ingredients');
                                $query_selectIngredient->where('id=' . $data['ingredient_id'][$i]);
                                $db->setQuery($query_selectIngredient);
                                $stack[] = (string) $query_selectIngredient;
                                $ingredientResult = $db->loadObjectList();
                            }

                            if (!empty($data['ingredient'][$i]) && $ingredientResult == false) {
                                $db = $this->getDbo();
                                $query_ingredient = $db->getQuery(true);
                                $query_ingredient->insert($db->quoteName('ingredients'));
                                $query_ingredient->columns(array('bezeichnung', 'quantity', 'measuringunit', 'recipe_id', 'artikel_nr'));
                                $query_ingredient->values("'" . htmlentities($data['ingredient'][$i]) . "','"
                                    . (int)$data['quantity'][$i] . "'," . $data['mesureunit'][$i] . ",'"
                                    . $insertedId . "',"
                                    . (int)$data['artikel_nr'][$i] . "");
                                $db->setQuery($query_ingredient);
                                $stack[] = (string)$query_ingredient;

                                $db->execute();
                            } else {
                                //If already exist, just update
                                $db = $this->getDbo();
                                $query_updateIngredient = $db->getQuery(true);
                                $query_updateIngredient->update('ingredients')
                                    ->set('bezeichnung'   .   ' = "' .htmlentities($data['ingredient'][$i]).'"')
                                    ->set('quantity'      . ' = "' . $data['quantity'][$i].'"')
                                    ->set('measuringunit' . ' = "' . $data['mesureunit'][$i].'"')
                                    ->set('artikel_nr'    . ' = "' . $data['artikel_nr'][$i].'"')
                                    ->where('id'          . ' = ' . $data['ingredient_id'][$i]);
                                $db->setQuery($query_updateIngredient);
                                $db->execute();
                            }
                        }
                        $i++;
                    }

                }catch (Exception $e)
                {
                    $this->setError($e->getMessage());

                    return false;
                }

                $this->saveImages($data, $insertedId);

            }

            // Clean the cache.
            $this->cleanCache();

            // Trigger the after save event.
            $dispatcher->trigger($this->event_after_save, array($context, $table, $isNew));
        }
        catch (Exception $e)
        {
            $this->setError($e->getMessage());

            return false;
        }
        if (isset($table->$key))
        {
            $this->setState($this->getName() . '.id', $table->$key);
        }

        $this->setState($this->getName() . '.new', $isNew);
        return true;

 }

    protected function saveRezeptKategorie($data, $lastInsertedId){
        //Look up, if row exists
        $db = $this->getDbo();
        $query_selectKat = $db->getQuery(true);
        $query_selectKat->select('kategorie_id','rezepte_id');
        $query_selectKat->from($db->quoteName('rezepte_kategorie'));
        $query_selectKat->where($db->quoteName('rezepte_id')."=".$lastInsertedId);
        $db->setQuery($query_selectKat);
        $kategorieResult = $db->loadObjectList();


        if($kategorieResult == false) {
            //if the row does not exit, add a row
            $db = $this->getDbo();
            $query_insertKat = $db->getQuery(true);
            $query_insertKat->insert($db->quoteName('rezepte_kategorie'));
            $query_insertKat->columns(array('kategorie_id', 'rezepte_id'));
            $query_insertKat->values("'" . $data['kategorie'] . "','" . $lastInsertedId . "'");
            $db->setQuery($query_insertKat);
            $db->execute();
        }else{
            //if the row exists, update the row
            if($kategorieResult->kategorie_id != $data['kategorie']) {
                $db = $this->getDbo();
                $query_updateKat = $db->getQuery(true);
                $query_updateKat->update($db->quoteName('rezepte_kategorie'))
                                ->set($db->quoteName('kategorie_id') . ' = ' . $db->quote($data['kategorie']))
                                ->where($db->quoteName('rezepte_id') . ' = ' . $db->quote($lastInsertedId));
                $db->setQuery($query_updateKat);
                $db->execute();
            }
        }
    }
}