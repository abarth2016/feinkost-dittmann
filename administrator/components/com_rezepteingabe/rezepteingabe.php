<?php
defined('_JEXEC') or die;
/* Einstiegsdatei, die einzige PHP-Datei, die Joomlabei der Aktivierung der Komponente direkt aufruft
 * Ab hier übergibt das Content-Management-System die Kontrolle an die Komponente
 * aktiviert den Komponenten
 */
if(!JFactory::getUser()->authorise('core.manage', 'com_rezepteingabe'))
{
    return JFactory::getApplication()->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR','error'));
}
$controller = JControllerLegacy::getInstance('RezeptEingabe');
$controller->execute(JFactory::getApplication()->input->get('task'));

$controller->redirect();