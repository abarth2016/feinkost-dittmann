<?php
defined('_JEXEC') or die;
/* verknüft alle Beszandteile der Listenansicht */
class RezepteingabeControllerRezepte extends JControllerAdmin
{
    //Vorbereiteter Code der es ermöglicht Detailoperationen auszuführen
    public function getModel($name = "Rezept", $prefix = 'RezepteingabeModel', $config = array('ignore_request' => true)){
        $model = parent::getModel($name, $prefix, $config);
        return $model;
    }
    public function delete()
    {
        parent::delete(); 
    }
}