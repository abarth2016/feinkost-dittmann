<?php
defined('_JEXEC') or die;

if(!JFactory::getUser()->authorise('core.manage', 'com_product'))
{
    return JFactory::getApplication()->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR','error'));
}
$controller = JControllerLegacy::getInstance('Product');
$controller->execute(JFactory::getApplication()->input->get('task'));

$controller->redirect();