<?php
defined('_JEXEC') or die;
/* View-Variante für das einzelne "Proukt"-Formular zur Neuanlage und Beaerbeitung */
class ProductViewProduct extends JViewLegacy
{
    protected $product;
    protected $form;

    public function display( $tpl=null ){

        $this->product = $this->get('Item');
        // Get the form
        $this->form = $this->get('Form');
        JFactory::getApplication()->input->set('hidemainmenu', true);
        JToolbarHelper::title(Jtext::_('COM_PRODUCT_MANAGER_REZEPT'),'');

        //Button hinzufügen
        JToolbarHelper::save('product.save');
        //JFactory::getApplication()->redirect('index.php?option=com_rezepteingabe&view=rezepte');


        if(empty($this->product->id))
        {
            JToolbarHelper::cancel('product.cancel', 'JTOOLBAR_CANCEL');
        }
        else
        {
            JToolbarHelper::cancel('product.cancel', 'JTOOLBAR_CLOSE');
        }

        parent::display($tpl);
    }
    /*   public function delete( $tpl=null ){

           parent::display($tpl);
       }*/

}