<?php
defined('_JEXEC') or die;
?>
<form action="<?php echo JRoute::_('index.php?option=com_product&view=product&layout=edit&id='.(int) $this->product->id);?>"
      method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('id');?></div>
                    <div class="controls"><?php echo html_entity_decode($this->form->getInput('id'));?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('marken_id');?></div>
                    <div class="controls"><?php echo html_entity_decode($this->form->getInput('marken_id'));?></div>
                </div>

                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('produktgruppen_id');?></div>
                    <div class="controls"><?php echo html_entity_decode($this->form->getInput('produktgruppen_id'));?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('web_produktgruppen_id');?></div>
                    <div class="controls"><?php echo html_entity_decode($this->form->getInput('web_produktgruppen_id'));?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('prodgroup_id');?></div>
                    <div class="controls"><?php echo html_entity_decode($this->form->getInput('prodgroup_id'));?></div>
                </div>
                <div class="control-group">
                    <input type="hidden" name="jform[bezeichnung_old]" value="<?php echo $this->product->bezeichnung;?>">
                    <div class="control-label"><?php echo $this->form->getLabel('bezeichnung');?></div>
                    <div class="controls"><?php echo $this->form->getInput('bezeichnung');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('beschreibung');?></div>
                    <div class="controls"><?php echo $this->form->getInput('beschreibung');?></div>
                </div>
                <div class="control-group">
                    <input type="hidden" name="jform[artikel_nr_old]" value="<?php echo $this->product->artikel_nr;?>">
                    <div class="control-label"><?php echo $this->form->getLabel('artikel_nr');?></div>
                    <div class="controls"><?php echo $this->form->getInput('artikel_nr');?></div>
                </div>
                <div class="control-group">
                    <input type="hidden" name="jform[fresh]" value="0">
                    <div class="control-label"><?php echo $this->form->getLabel('fresh');?></div>
                    <div class="controls"><?php echo $this->form->getInput('fresh');?></div>
                </div>
                <div class="control-group">
                    <input type="hidden" name="jform[bio]" value="0">
                    <div class="control-label"><?php echo $this->form->getLabel('bio');?></div>
                    <div class="controls"><?php echo $this->form->getInput('bio');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('menge');?></div>
                    <div class="controls"><?php echo $this->form->getInput('menge');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('verpackungseinheit');?></div>
                    <div class="controls"><?php echo $this->form->getInput('verpackungseinheit');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('verpackungen_id');?></div>
                    <div class="controls"><?php echo html_entity_decode($this->form->getInput('verpackungen_id'));?></div>
                </div>
                <div class="control-group">
                    <input type="hidden" name="jform[kuehlpflichtig]" value="0">
                    <div class="control-label"><?php echo $this->form->getLabel('kuehlpflichtig');?></div>
                    <div class="controls"><?php echo $this->form->getInput('kuehlpflichtig');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('restlagerzeit_tage');?></div>
                    <div class="controls"><?php echo $this->form->getInput('restlagerzeit_tage');?></div>
                </div>
                <div  class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('ean');?></div>
                    <div class="controls"><?php echo $this->form->getInput('ean');?></div>
                </div>
                <div  class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('ean_verpackung');?></div>
                    <div class="controls"><?php echo $this->form->getInput('ean_verpackung');?></div>
                </div>
                <div  class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('markteinfuehrung');?></div>
                    <div class="controls"><?php echo $this->form->getInput('markteinfuehrung');?></div>
                </div>
                <div  class="control-group">
                    <input type="hidden" name="jform[published]" value="0">
                    <div class="control-label"><?php echo $this->form->getLabel('published');?></div>
                    <div class="controls"><?php echo $this->form->getInput('published');?></div>
                </div>
                <div class="control-group">
                    <input type="hidden" name="jform[productimage_normal_old]" value="<?php  echo './../images' . DIRECTORY_SEPARATOR . 'produktbilder' . DIRECTORY_SEPARATOR . '275_376' . DIRECTORY_SEPARATOR . $this->product->filename;?>">
                    <div class="control-label"><?php echo $this->form->getLabel('productimage_normal');?></div>
                    <div class="controls"><?php echo $this->form->getInput('productimage_normal');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('prod_id_shop');?></div>
                    <div class="controls"><?php echo $this->form->getInput('prod_id_shop');?></div>
                </div>
                <input type="hidden" name="task" value="">
                <?php echo JHtml::_('form.token');?>
            </fieldset>
            <?php if(isset($this->product->filename)){?>
            Sucht nach Bilder mit folgendem Namen: <?php echo $this->product->filename;?>

            <?php
            if($this->product->filename) {
                $images[] = './../images' . DIRECTORY_SEPARATOR . 'produktbilder' . DIRECTORY_SEPARATOR . '275_376' . DIRECTORY_SEPARATOR . $this->product->filename;

                echo "<ul style='list-style-type: none'>";
                foreach ($images as $image) {
                    if (file_exists($image)) {
                        echo "<li style='clear:both'>" . $image . "</li>";
                        echo '<input name type="hidden" value="' . $image . '">';
                        echo '<li style="clear:both;float:left"><img style="margin-bottom:20px;border:1px dotted grey" src="' . $image . '" style="max-width:500px" alt="' . $image . '"></li>';
                    }
                }
                echo "</ul>";
            }
            ?>
            <?php } ?>
        </div>
    </div>
</form>
