<?php
defined('_JEXEC') or die;
/* view.html.php erhält die Daten aus dem Model, bereitet sie ggf. auf und reicht sie an das Template default.php weiter */
class ProductViewProducts extends JViewLegacy{
    protected $products;

    public function display($tpl = null)
    {
        //holt sich alle Daten aus dem Model und stellt sie dem Template in der Variablen $recipes zur Verfügung
        $this->products = $this->get('Items');
        $this->pagination = $this->get('Pagination');
        
        $bar = JToolBar::getInstance('toolbar');

        JToolbarHelper::title(JText::_('COM_PRODUCT_MANAGER_PRODUCTS'));
        JToolbarHelper::addNew('product.add');
        JToolbarHelper::editList('product.edit');
        JToolbarHelper::deleteList(JText::_('COM_PRODUCT_DELETE_CONFIRMATION'), 'products.delete', 'JTOOLBAR_DELETE');

        parent::display($tpl);
    }

}