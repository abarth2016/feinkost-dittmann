<?php
/*
 * 'id',
                  'marken_id',
                  'verpackungen_id',
                  'produktgruppen_id',
                  'web_produktgruppen_id',
                  'bezeichnung',
                  'beschreibung',
                  'artikel_nr',
                  'fresh',
                  'bio',
                  'menge',
                  'verpackungseinheit',
                  'kuehlpflichtig',
                  'restlagerzeit_tage',
                  'ean',
                  'ean_verpackung',
                  'markteinfuehrung',
                  'published')));
 */
defined('_JEXEC') or die;
?>
<form action="<?php echo JRoute::_('index.php?option=com_product&view=products')?>" method="post" name="adminForm" id="adminForm">
    <div id="j-main-container">
        <div class="clearfix"></div>
        <table class="table table-striped" id="productsList">
            <thead>
            <tr>
                <th class="nowrap center">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL');?>" onClick="Joomla.checkAll(this)">
                </th>
                <th>
                    <?php echo JText::_('COM_PRODUCT_COLUMN_HEADER_ID');?>
                </th>
                <th>
                    <?php echo JText::_('COM_PRODUCT_COLUMN_HEADER_BEZEICHNUNG');?>
                </th>
                <th>
                    <?php echo JText::_('COM_PRODUCT_COLUMN_HEADER_PRODGRP_ID');?>
                </th>
                <th>
                    <?php echo JText::_('COM_PRODUCT_COLUMN_HEADER_VERPACKUNGSEINHEIT');?>
                </th>
                <th>
                    <?php echo JText::_('COM_PRODUCT_COLUMN_ARTIKEL_NR');?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($this->products as $i=>$product): ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="nowrap center">
                        <?php  echo JHtml::_('grid.id', $i, $product->produkt_id);?>
                    </td>
                    <td class="nowrap center">
                        <?php  echo  $product->produkt_id;?>
                    </td>
                    <td class="has-context">
                        <a href="<?php echo JRoute::_('index.php?option=com_product&task=product.edit&id='.(int) $product->produkt_id);?>">
                            <?php echo $this->escape(html_entity_decode($product->produkt_bezeichnung));?>
                        </a>
                    </td>
                    <td class="small">
                        <?php echo $this->escape($product->produktgruppe);?>
                    </td>
                    <td>
                        <?php echo $this->escape($product->verpackungsbeschreibung)." (".$product->verpackungseinheit.")";?>

                    </td>
                    <td>
                        <?php echo $this->escape($product->artikel_nr);?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>

        <input type="hidden" name="task" value="">
        <input type="hidden" name="boxchecked" value="0">
        <?php echo JHtml::_('form.token');?>
        <?php echo $this->pagination->getListFooter(); ?>

    </div>
</form>
