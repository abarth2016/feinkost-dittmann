<?php
defined('_JEXEC') or die;
/* Standard-Model für die Datenbankabfrage aller Datenbankeinträge der products-Tabelle */

class ProductModelProduct extends JModelAdmin
{
    /* gets data to show a list of all products */
    protected function getListQuery()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
            array('id',
                'marken_id',
                'verpackungen_id',
                'produktgruppen_id',
                'produkte_produktgruppen.produkt_id',
                'bezeichnung',
                'beschreibung',
                'artikel_nr',
                'fresh',
                'bio',
                'menge',
                'verpackungseinheit',
                'kuehlpflichtig',
                'restlagerzeit_tage',
                'ean',
                'ean_verpackung',
                'markteinfuehrung',
                'published'),array('id',
            'marken_id',
            'verpackungen_id',
            'produktgruppen_id',
            'web_produktgruppen_id',
            'bezeichnung',
            'beschreibung',
            'artikel_nr',
            'fresh',
            'bio',
            'menge',
            'verpackungseinheit',
            'kuehlpflichtig',
            'restlagerzeit_tage',
            'ean',
            'ean_verpackung',
            'markteinfuehrung',
            'published')));
        $query->from($db->quoteName('produkte'));
        $query->join('LEFT', 'produkte_produktgruppen on produkte.id = produkte_produktgruppen.produkt_id');
        $query->join('LEFT', 'produktgruppen on produkte_produktgruppen.produktgruppen_id = produktgruppen.id');

        $query->order('produkte.id DESC');

        return $query;

    }
    /* returns a product number used in the dittmann shop by an articleId
    */
    protected function getProdNumber( $prodId )
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
                       array('magento_id')));
        $query->from($db->quoteName('produkte_artikel'));
        $query->where($db->quoteName('artikel_nr')."=". $prodId);
        $db->setQuery( $query);
        $row = $db->loadObjectList();
        if(is_array($row) && isset($row[0]) && is_object($row[0])) {
            return $row[0]->magento_id;
        }
    }
    /*
     * saves a row to assign a product to a group
     */
    protected function saveProduGroupAssignment($prodId,$prodGrpId){
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->insert($db->quoteName('produkte_produktgruppen'));
        $query->columns(array('produkt_id', 'produktgruppen_id'));
        $query->values($prodId.",". $prodGrpId);
        $db->setQuery($query);
        //echo($query->__toString());die();

        $db->execute();
    }

    /* updates entry in table produkte_produktgruppen, if produgrp_id is changed
*/
    protected function updateGroupAssignment($prodId = null,$prodGrpId){
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->update($db->quoteName('produkte_produktgruppen'));
        $query->set(array('produktgruppen_id='.$prodGrpId));
        $query->where('produkt_id='.$prodId);
        $db->setQuery($query);
        //echo($query->__toString());die();

        $db->execute();
    }

    /* saves the shop_id for an article in produkte_article
    */
    protected function saveProdNumber($prodIdShop, $artNr){
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->insert($db->quoteName('produkte_artikel'));
        $query->columns(array('magento_id', 'artikel_nr'));
        $query->values($prodIdShop.",". $artNr);
        $db->setQuery($query);
        $db->execute();
    }

    /* updates entry in table produkte_article, if shop_id is changed
    */
    protected function updateProdNumber($prodIdShop, $artNr){
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->update($db->quoteName('produkte_artikel'));
        $query->set(array('magento_id='.$prodIdShop));
        $query->where('artikel_nr='. $artNr);
        $db->setQuery($query);
        $db->execute();
    }

    /* return table produkte
    */
    public function getTable($name = 'Product', $prefix = 'ProductTable', $options = array())
    {
        return parent::getTable($name, $prefix, $options);
    }

    /* standard function to return an standard form */
    public function getForm($data = array(), $loadData = true)
    {
        $app = JFactory::getApplication();
        $form = $this->loadForm('com_product.product','product', array('control'=>'jform','load_data' =>$loadData));

        if(empty($form))
        {
            return false;
        }
        return $form;
    }

    /* return data to show form to edit one product
    */
    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState('com_product.edit.product.data', array());

        if(empty($data)){
            $data = $this->getItem();
            if($data->bezeichnung){
                $createdFileName = $this->createSpeakingFilename($data->bezeichnung,$data->artikel_nr, "png");
                $data->filename = $createdFileName;
            }
            if(!empty($data->artikel_nr)) {
                $data->prod_id_shop = $this->getProdNumber($data->artikel_nr);
            }
        }
        return $data;
    }

    protected function getProdGroupId($pk)
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
            array('produktgruppen_id')));
        $query->from($db->quoteName('produkte_produktgruppen'));
        if (isset($pk)) {
        
        $query->where($db->quoteName('produkt_id') . "=" . $pk);
    }

        $db->setQuery($query);
        return $db->loadObjectList();
    }
    /* returns one product
    */
    public function getItem($pk = null)
    {
        $result = parent::getItem($pk);
        $prodGroupResult = $this->getProdGroupId($result->id);
        if(!empty($prodGroupResult[0])){
        $result->prodgroup_id = $prodGroupResult[0]->produktgruppen_id;
        }

        if ($result->bezeichnung) {
            $createdFileName = $this->createSpeakingFilename($result->bezeichnung, $result->artikel_nr, "png");
            $result->filename = $createdFileName;
        }
        return $result;
    }

    /*
     * saves the images for the product
     */
    protected function saveImages($data, $artNo)
    {
        //var_dump($data);die();
        //echo $data['bezeichnung']."\n";
        //echo $data['bezeichnung_old']."\n";
        //echo "AA".(int) strcmp ($data['bezeichnung'], $data['bezeichnung_old']);
        //echo "-".$data['artikel_nr']."\n";
        //echo "-".$data['artikel_nr_old']."\n";
        //echo "-".strcmp ($data['artikel_nr'], $data['artikel_nr_old']);
        //die();
        $createdFileName = $this->createSpeakingFilename(html_entity_decode($data['bezeichnung']), $artNo, 'png');
        $destination_normal = "../images/produktbilder/275_376/" . $createdFileName;
        $destination_normal_old = $data['productimage_normal_old'];

        //275x276
        if (!empty($_FILES['jform']['tmp_name']['productimage_normal'])) {

            if (!move_uploaded_file(
                $_FILES['jform']['tmp_name']['productimage_normal'],
                $destination_normal)
            ) {

                $this->setError("productimage_normal to $destination_normal could not be copied");
            }
        }elseif(!empty($destination_normal_old) && ((strcmp ($data['bezeichnung'], $data['bezeichnung_old'])!=0) || (strcmp($data['artikel_nr'], $data['artikel_nr_old']) != 0))){

            if (!copy(
                $destination_normal_old,
                $destination_normal)
            ) {
                $this->setError("productimage_normal to $destination_normal could not be copied");
            }

        }

    }
    /* creates a file name from headline
    */
    protected function createSpeakingFilename(  $headline, $prodId, $fileextension = "jpg"){
        $string = mb_strtolower($headline);
        $string = preg_replace('/\x{00e4}/u', "ae", $string);
        $string = preg_replace('/\x{00fc}/u', "ue", $string);
        $string = preg_replace('/\x{00f6}/u', "oe", $string);
        $string = preg_replace('/\x{00df}/u', "ss", $string);
        $string = preg_replace('/\r\n/', "_", $string);
        $string = preg_replace('/\n/', "_", $string);
        $string = preg_replace('/\r/', "_", $string);
        $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
        $string = preg_replace("/-/", "_", $string);
        $string = preg_replace('/\x{0020}/u', "_", $string);
        $createdFileName = preg_replace("/[^a-z0-9_]/","",$string);
        $destination = $createdFileName."_" . str_pad($prodId, 4, 0) . "." . $fileextension;
        return $destination;
    }

    /*
     * delets the entry
     */
    public function delete(&$pk){

        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
                                        array('produkte.artikel_nr')));
        $query->from('produkte');
        $query->where('produkte.id='.$pk[0]);
        $db->setQuery($query);
        $row = $db->loadObjectList();

        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
            array('id')));
        $query->from('produkte_artikel');
        $query->where('artikel_nr='.$row[0]->artikel_nr);
        $db->setQuery($query);
        $row2 = $db->loadObjectList();



        //delete entry in produkte_artikel also
        if(isset($row2[0]->id) ) {
            $db = $this->getDbo();
            $query_recCat = $db->getQuery(true);
            $query_recCat->delete('produkte_artikel');
            $query_recCat->where('id=' . $row2[0]->id);
            $db->setQuery($query_recCat);
            if(!$db->execute()){
                throw new Exception('An error occured while trying to execute query:"'.$query.'"');
            }
        }
        parent::delete( $pk);


    }

    /*
     * saves the product
     */
    public function save($data){
        parent::save($data);
        
        $pk = (!empty($data['id'])) ? $data['id'] : (int) $this->getState($this->getName() . '.id');
        $artNo = (!empty($data['artikel_nr'])) ? $data['artikel_nr'] : (int) $this->getState($this->getName() . '.artikel_nr');
        echo $artNo;
        $this->saveImages($data, $artNo);


        /* edit produkte_artikel for linking to shop */
        //Check, if there is already one entry for this artikel_nr in table produkte_artikel
        $shopNr = $this->getProdNumber($data['artikel_nr']);

        if(isset($data['artikel_nr']) && $data['prod_id_shop']) {
            if (!empty($shopNr)) {
                //update produkte_artikel to link product to shop
                $this->updateProdNumber($data['prod_id_shop'], $data['artikel_nr']);
            } else {
                //insert row into produkte_artikel to link product to shop
                $this->saveProdNumber($data['prod_id_shop'], $data['artikel_nr']);
            }
        }
        $prodGroup = $this->getProdGroupId($data['id']);
        if (isset($prodGroup[0]->produktgruppen_id)){
            $this->updateGroupAssignment($data['id'],$data['prodgroup_id']);
        }else{
            $this->saveProduGroupAssignment($data['id'],$data['prodgroup_id']);
        }
 
        return true;

    }
}