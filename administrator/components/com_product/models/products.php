<?php
defined('_JEXEC') or die;
/* Standard-Model für die Datenbankabfrage aller Datenbankeinträge der products-Tabelle */
class ProductModelProducts extends JModelList
{
    protected function getListQuery()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
            array('produkte.id',
                  'produkte.marken_id',
                  'verpackungen_id',
                  'produktgruppen_id',
                  'web_produktgruppen_id',
                  'produkte.bezeichnung',
                  'produkte.beschreibung',
                  'artikel_nr',
                  'fresh',
                  'bio',
                  'menge',
                  'verpackungseinheit',
                  'kuehlpflichtig',
                  'restlagerzeit_tage',
                  'ean',
                  'ean_verpackung',
                  'markteinfuehrung',
                  'published',
                   'marken.bezeichnung',
                   'produktgruppen.bezeichnung',
                  'verpackungen.bezeichnung'),
            array('produkt_id',
            null,
            null,//'verpackungen_id',
            null,//'produktgruppen_id',
            null,//'web_produktgruppen_id',
            'produkt_bezeichnung',//'produkte.bezeichnung',
            'produkt_beschreibung',//produkte.beschreibung',
            null,//'artikel_nr',
            null,//'fresh',
            null,//'bio',
            null,//'menge',
            null,//'verpackungseinheit',
            null,//'kuehlpflichtig',
            null,//'restlagerzeit_tage',
            null,//'ean',
            null,//'ean_verpackung',
            null,//'markteinfuehrung',
            null,//'published',
            null,//'marken.bezeichnung
                'produktgruppe',
                'verpackungsbeschreibung'
            )));
        $query->from($db->quoteName('produkte'));
        $query->join('LEFT', 'marken on produkte.marken_id=marken.id');
        $query->join('LEFT', 'produktgruppen on produkte.produktgruppen_id=produktgruppen.id');
        $query->join('LEFT', 'verpackungen on produkte.verpackungen_id=verpackungen.id');

        $query->order('produkte.id DESC');

        return $query;
    }
}