<?php
defined('_JEXEC') or die;
/* verknüft alle Beszandteile der Listenansicht */
class ProductControllerProducts extends JControllerAdmin
{
    //Vorbereiteter Code der es ermöglicht Detailoperationen auszuführen
    public function getModel($name = "Product", $prefix = 'ProductModel', $config = array('ignore_request' => true)){
        $model = parent::getModel($name, $prefix, $config);
        return $model;
    }
    public function delete($pk)
    {
        parent::delete($pk);
    }
}