<?php
defined('_JEXEC') or die;

if(!JFactory::getUser()->authorise('core.manage', 'com_productgroup'))
{
    return JFactory::getApplication()->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR','error'));
}
$controller = JControllerLegacy::getInstance('Productgroup');
$controller->execute(JFactory::getApplication()->input->get('task'));

$controller->redirect();