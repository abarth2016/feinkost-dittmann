<?php
defined('_JEXEC') or die;
/* Standard-Model für die Datenbankabfrage aller Datenbankeinträge der products-Tabelle */
class ProductgroupModelProductgroups extends JModelList
{
    protected function getListQuery()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
            array('produktgruppen.id',
                  'produktgruppen.bezeichnung',
                  ),
            array('id',
            'bezeichnung',

            )));
        $query->from($db->quoteName('produktgruppen'));
        $query->order('produktgruppen.id DESC');
        return $query;
    }
}