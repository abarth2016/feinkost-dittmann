<?php
defined('_JEXEC') or die;
class ProductgroupModelProductgroup extends JModelAdmin
{
    /* gets data to show a list of all productgroups */
    protected function getListQuery()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
            array('id',
                'bezeichnung')));
        $query->from($db->quoteName('produktgruppen'));
        $query->order('produktgruppen.id DESC');

        return $query;
    }
    /*
     * saves a row to assign a product to a group
     */
    protected function saveWeltenAssignment($weltenId,$prodGrpId){
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->insert($db->quoteName('welten_produktgruppen'));
        $query->columns(array('welten_id', 'produktgruppen_id'));
        $query->values($weltenId.",". $prodGrpId);
        $db->setQuery($query);
        $db->execute();
    }

    /* updates entry in table produkte_produktgruppen, if produgrp_id is changed
*/
    protected function updateWeltenAssignment($weltenId = null,$prodGrpId){
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->update($db->quoteName('welten_produktgruppen'));
        $query->set(array('welten_id='.$weltenId));
        $query->where('produktgruppen_id='.$prodGrpId);
        $db->setQuery($query);
        $db->execute();
    }
    /* return table produkte
    */
    public function getTable($name = 'Productgroup', $prefix = 'ProductgroupTable', $options = array())
    {
        return parent::getTable($name, $prefix, $options);
    }

    /* standard function to return an standard form */
    public function getForm($data = array(), $loadData = true)
    {
        $app = JFactory::getApplication();
        $form = $this->loadForm('com_productgroup.productgroup','productgroup', array('control'=>'jform','load_data' =>$loadData));

        if(empty($form))
        {
            return false;
        }
        return $form;
    }

    /* return data to show form to edit one product
    */
    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState('com_productgroup.edit.productgroup.data', array());

        if(empty($data)){
            $data = $this->getItem();

            $weltenId = $this->getProductCategory($data->id);
            if(isset($weltenId[0])) {
                $data->welten_id = $weltenId[0]->welten_id;
            }

            if($data->bezeichnung){
                $createdFileName = $this->createSpeakingFilename($data->bezeichnung,$data->id, "png");
                $data->filename = $createdFileName;
            }
        }
        return $data;
    }

    protected function getProductCategory($id){
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
            array('welten_id')));
        $query->from($db->quoteName('welten_produktgruppen'));
        if (isset ($id)) {
            $query->where($db->quoteName('produktgruppen_id') . "=" . $id);
        }
        $db->setQuery($query);
        return $db->loadObjectList();

    }
    /* returns one product
    */
    public function getItem($pk = null)
    {
        $result = parent::getItem($pk);
	    $prodGroupResult = $this->getProductCategory($result->id);
        if(!empty($prodGroupResult[0])){
        $result->welten_id = $prodGroupResult[0]->welten_id;
        }
        if ($result->bezeichnung) {
            $createdFileName = $this->createSpeakingFilename($result->bezeichnung, $result->id, "png");
            $result->filename = $createdFileName;

        }
        return $result;
    }

    /*
     * saves the images for the product
     */
    protected function saveImages($data, $id)
    {
        $createdFileName = $this->createSpeakingFilename(html_entity_decode($data['bezeichnung']), $id, 'png');
        $destination = "../images/produktgruppen/275_376/" . $createdFileName;


        if (!empty($_FILES['jform']['tmp_name']['productgroup_image'])) {
            if (!move_uploaded_file(
                $_FILES['jform']['tmp_name']['productgroup_image'],
                $destination)
            ) {
                $this->setError("productgroup_image to $destination could not be coppied");

            }
        }

    }
    /* creates a file name from headline
    */
    protected function createSpeakingFilename(  $headline, $prodId, $fileextension = "jpg"){
        $string = mb_strtolower($headline);
        $string = preg_replace('/\x{00e4}/u', "ae", $string);
        $string = preg_replace('/\x{00fc}/u', "ue", $string);
        $string = preg_replace('/\x{00f6}/u', "oe", $string);
        $string = preg_replace('/\x{00df}/u', "ss", $string);
        $string = preg_replace('/\r\n/', "_", $string);
        $string = preg_replace('/\n/', "_", $string);
        $string = preg_replace('/\r/', "_", $string);
        $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
        $string = preg_replace("/-/", "_", $string);
        $string = preg_replace('/\x{0020}/u', "_", $string);
        $createdFileName = preg_replace("/[^a-z0-9_]/","",$string);
        $destination = $createdFileName."_" . str_pad($prodId, 3, "0",STR_PAD_LEFT) . "." . $fileextension;
        return $destination;
    }

  

    /*
     * saves the product
     */
    public function save($data){
        parent::save($data);
        $pk = (!empty($data['id'])) ? $data['id'] : (int) $this->getState($this->getName() . '.id');
        $this->saveImages($data, $pk);

        if($data['id'] != 0) {
            //Do not call, if data is inserted for the firt time
            $prodGroup = $this->getProductCategory($data['id']);
            $this->updateWeltenAssignment($data['welten_id'], $data['id']);
        }else{
            $this->saveWeltenAssignment($data['welten_id'], $pk);
        }
        return true;

    }
}