<?php
defined('_JEXEC') or die;
/* verknüft alle Beszandteile der Listenansicht */
class ProductgroupControllerProductgroups extends JControllerAdmin
{
    //Vorbereiteter Code der es ermöglicht Detailoperationen auszuführen
    public function getModel($name = "Productgroup", $prefix = 'ProductgroupModel', $config = array('ignore_request' => true)){
        $model = parent::getModel($name, $prefix, $config);
        return $model;
    }
    public function delete()
    {
        parent::delete();
    }
}