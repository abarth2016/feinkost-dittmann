<?php
defined('_JEXEC') or die;

class ProductgroupController extends JControllerLegacy
{
    //wenn default_view nicht gesetzt wurde und kein View-Angabe mit übergeben wurde wird versucht den Komponentennamen als Ansicht aufzurufen
    protected $default_view = "productgroups";

    public function display($cachable = false, $urlparams = false)
    {
        //vorereiteter Code hier können gezielte Anweisungen audgeführt werden
        //wenn eine bestimmte LAyout-View-Kombination zur Verfügung gestellt wird
        $view = $this->input->get('view', 'productgroups');
        $layout = $this->input->get('layout', 'default');
        $id = $this->input->getInt('id');
        $task = $this->input->get('task');


        if ($view == 'productgroup' && ($layout == 'edit') && !$this->checkEditId('com_productgroup.edit.productgroup', $id)) {
            // Somehow the person just went to the form - we don't allow that.
            $this->setMessage($this->getError(), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_productgroup&view=productgroup&id=' . $id, false));

            return false;
        }
        
        parent::display();

        return $this;
    }
    /**
     * Save the incoming data and then return to the Browse task
     *
     * @return  bool
     */

}