<?php
defined('_JEXEC') or die;
/* view.html.php erhält die Daten aus dem Model, bereitet sie ggf. auf und reicht sie an das Template default.php weiter */
class ProductgroupViewProductgroups extends JViewLegacy{
    protected $productgroups;

    public function display($tpl = null)
    {
        //holt sich alle Daten aus dem Model und stellt sie dem Template in der Variablen $recipes zur Verfügung
        $this->productgroups = $this->get('Items');
        
        $this->pagination = $this->get('Pagination');
        
        $bar = JToolBar::getInstance('toolbar');

        JToolbarHelper::title(JText::_('COM_PRODUCTGROUP_MANAGER_PRODUCTGROUPS'));
        JToolbarHelper::addNew('productgroup.add');
        JToolbarHelper::editList('productgroup.edit');
        JToolbarHelper::deleteList(JText::_('COM_PRODUCTGROUP_DELETE_CONFIRMATION'), 'productgroups.delete', 'JTOOLBAR_DELETE');

        parent::display($tpl);
    }

}