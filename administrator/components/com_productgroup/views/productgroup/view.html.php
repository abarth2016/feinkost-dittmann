<?php
defined('_JEXEC') or die;
/* View-Variante für das einzelne "Produktgruppen"-Formular zur Neuanlage und Beaerbeitung */
class ProductgroupViewProductgroup extends JViewLegacy
{
    protected $productgroup;
    protected $form;

    public function display( $tpl=null ){

        $this->productgroup = $this->get('Item');
        // Get the form
        $this->form = $this->get('Form');
        JFactory::getApplication()->input->set('hidemainmenu', true);
        JToolbarHelper::title(Jtext::_('COM_PRODUCTGROUP_MANAGER_PRODUCTGROUP'),'');

        //Button hinzufügen
        JToolbarHelper::save('productgroup.save');
        JToolbarHelper::deleteList(JText::_('COM_PRODUCTGROUP_DELETE_CONFIRMATION'), 'productgroups.delete', 'JTOOLBAR_DELETE');

        if(empty($this->productgroup->id))
        {
            JToolbarHelper::cancel('productgroup.cancel', 'JTOOLBAR_CANCEL');
        }
        else
        {
            JToolbarHelper::cancel('productgroup.cancel', 'JTOOLBAR_CLOSE');
        }

        parent::display($tpl);
    }

}