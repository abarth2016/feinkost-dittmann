<?php
defined('_JEXEC') or die;
?>
<form action="<?php echo JRoute::_('index.php?option=com_productgroup&view=productgroup&layout=edit&id='.(int) $this->productgroup->id);?>"
      method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('id');?></div>
                    <div class="controls"><?php echo html_entity_decode($this->form->getInput('id'));?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('bezeichnung');?></div>
                    <div class="controls"><?php echo $this->form->getInput('bezeichnung');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('welten_id');?></div>
                    <div class="controls"><?php echo $this->form->getInput('welten_id');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('productgroup_image');?></div>
                    <div class="controls"><?php echo $this->form->getInput('productgroup_image');?></div>
                </div>
                <input type="hidden" name="task" value="">
                <?php echo JHtml::_('form.token');?>
            </fieldset>
            <?php
if(isset($this->productgroup->filename)){
            echo "Looks for image named: ".$this->productgroup->filename;

            if($this->productgroup->filename) {
                $images[] = './../images' . DIRECTORY_SEPARATOR . 'produktgruppen' . DIRECTORY_SEPARATOR . '275_376' . DIRECTORY_SEPARATOR . $this->productgroup->filename;

                echo "<ul style='list-style-type: none'>";
                foreach ($images as $image) {
                    if (file_exists($image)) {
                        echo '<li style="clear:both;float:left"><img style="margin-bottom:20px;border:1px dotted grey" src="' . $image . '" style="max-width:500px" alt="' . $image . '"></li>';
                    }
                }
                echo "</ul>";
            }
}
            ?>
        </div>
    </div>
</form>
