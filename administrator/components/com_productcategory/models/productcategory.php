<?php
defined('_JEXEC') or die;
class ProductcategoryModelProductcategory extends JModelAdmin
{
    /* gets data to show a list of all productcategories */
    protected function getListQuery()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
            array('id',
                'bezeichnung')));
        $query->from($db->quoteName('welten'));
        $query->order('welten.id DESC');

        return $query;
    }

    /* return table produkte
    */
    public function getTable($name = 'Productcategory', $prefix = 'ProductcategoryTable', $options = array())
    {
        return parent::getTable($name, $prefix, $options);
    }

    /* standard function to return an standard form */
    public function getForm($data = array(), $loadData = true)
    {
        $app = JFactory::getApplication();
        $form = $this->loadForm('com_productcategory.productcategory','productcategory', array('control'=>'jform','load_data' =>$loadData));

        if(empty($form))
        {
            return false;
        }
        return $form;
    }

    /* return data to show form to edit one product
    */
    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState('com_productcategory.edit.productcategory.data', array());

        if(empty($data)){
            $data = $this->getItem();

            if($data->bezeichnung){
                $createdFileName = $this->createSpeakingFilename($data->bezeichnung,$data->id, "png");
                $data->filename = $createdFileName;
            }
        }
        return $data;
    }

    /* returns one product
    */
    public function getItem($pk = null)
    {
        $result = parent::getItem($pk);
        if ($result->bezeichnung) {
            $createdFileName = $this->createSpeakingFilename($result->bezeichnung, $result->id, "png");
            $result->filename = $createdFileName;

        }
        return $result;
    }

    /*
     * saves the images for the product
     */
    protected function saveImages($data, $id)
    {
        $createdFileName = $this->createSpeakingFilename(html_entity_decode($data['bezeichnung']), $id, 'png');
        $destination = "../images/produktkategorien/275_376/" . $createdFileName;


        if (!empty($_FILES['jform']['tmp_name']['productcategory_image'])) {
            if (!move_uploaded_file(
                $_FILES['jform']['tmp_name']['productcategory_image'],
                $destination)
            ) {
                $this->setError("productcategory_image to $destination could not be coppied");

            }
        }

    }
    /* creates a file name from headline
    */
    protected function createSpeakingFilename(  $headline, $prodId, $fileextension = "jpg"){
        $string = mb_strtolower($headline);
        $string = preg_replace('/\x{00e4}/u', "ae", $string);
        $string = preg_replace('/\x{00fc}/u', "ue", $string);
        $string = preg_replace('/\x{00f6}/u', "oe", $string);
        $string = preg_replace('/\x{00df}/u', "ss", $string);
        $string = preg_replace('/\r\n/', "_", $string);
        $string = preg_replace('/\n/', "_", $string);
        $string = preg_replace('/\r/', "_", $string);
        $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
        $string = preg_replace("/-/", "_", $string);
        $string = preg_replace('/\x{0020}/u', "_", $string);
        $createdFileName = preg_replace("/[^a-z0-9_]/","",$string);
        $destination = $createdFileName."_" . str_pad($prodId, 3, "0",STR_PAD_LEFT) . "." . $fileextension;
        return $destination;
    }

  

    /*
     * saves the product
     */
    public function save($data){
        parent::save($data);
        $pk = (!empty($data['id'])) ? $data['id'] : (int) $this->getState($this->getName() . '.id');
        $this->saveImages($data, $pk);

        return true;

    }
}