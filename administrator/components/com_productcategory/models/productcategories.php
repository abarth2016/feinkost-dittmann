<?php
defined('_JEXEC') or die;
/* Standard-Model für die Datenbankabfrage aller Datenbankeinträge der products-Tabelle */
class ProductcategoryModelProductcategories extends JModelList
{
    protected function getListQuery()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(
            array('welten.id',
                  'welten.bezeichnung',
                  ),
            array('id',
            'bezeichnung',

            )));
        $query->from($db->quoteName('welten'));
        $query->order('welten.id DESC');
        return $query;
    }
}