<?php
defined('_JEXEC') or die;
/* View-Variante für das einzelne "Produktgruppen"-Formular zur Neuanlage und Beaerbeitung */
class ProductcategoryViewProductcategory extends JViewLegacy
{
    protected $productcategory;
    protected $form;

    public function display( $tpl=null ){

        $this->productcategory = $this->get('Item');
        // Get the form
        $this->form = $this->get('Form');
        JFactory::getApplication()->input->set('hidemainmenu', true);
        JToolbarHelper::title(Jtext::_('COM_PRODUCTCATEGORY_MANAGER_PRODUCTCATEGORY'),'');

        //Button hinzufügen
        JToolbarHelper::save('productcategory.save');
        JToolbarHelper::deleteList(JText::_('COM_PRODUCTCATEGORY_DELETE_CONFIRMATION'), 'productcategories.delete', 'JTOOLBAR_DELETE');

        if(empty($this->productcategory->id))
        {
            JToolbarHelper::cancel('productcategory.cancel', 'JTOOLBAR_CANCEL');
        }
        else
        {
            JToolbarHelper::cancel('productcategory.cancel', 'JTOOLBAR_CLOSE');
        }

        parent::display($tpl);
    }

}