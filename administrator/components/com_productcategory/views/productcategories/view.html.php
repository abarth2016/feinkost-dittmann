<?php
defined('_JEXEC') or die;
/* view.html.php erhält die Daten aus dem Model, bereitet sie ggf. auf und reicht sie an das Template default.php weiter */
class ProductcategoryViewProductcategories extends JViewLegacy{
    protected $productcategories;

    public function display($tpl = null)
    {
        //holt sich alle Daten aus dem Model und stellt sie dem Template in der Variablen $recipes zur Verfügung
        $this->productcategories = $this->get('Items');
        //var_dump($this->productcategories);
        $this->pagination = $this->get('Pagination');
        
        $bar = JToolBar::getInstance('toolbar');

        JToolbarHelper::title(JText::_('COM_PRODUCTCATEGORY_MANAGER_PRODUCTCATEGORIES'));
        JToolbarHelper::addNew('productcategory.add');
        JToolbarHelper::editList('productcategory.edit');
        JToolbarHelper::deleteList(JText::_('COM_PRODUCTCATEGORY_DELETE_CONFIRMATION'), 'productcategories.delete', 'JTOOLBAR_DELETE');

        parent::display($tpl);
    }

}