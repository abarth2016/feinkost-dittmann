<?php
defined('_JEXEC') or die;
?>
<form action="<?php echo JRoute::_('index.php?option=com_productcategory&view=productcategories')?>" method="post" name="adminForm" id="adminForm">
    <div id="j-main-container">
        <div class="clearfix"></div>
        <table class="table table-striped" id="productgroupsList">
            <thead>
            <tr>
                <th class="nowrap center">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL');?>" onClick="Joomla.checkAll(this)">
                </th>
                <th>
                    <?php echo JText::_('COM_PRODUCTCATEGORIES_COLUMN_HEADER_ID');?>
                </th>
                <th>
                    <?php echo JText::_('COM_PRODUCTCATEGORIES_COLUMN_HEADER_BEZEICHNUNG');?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($this->productcategories as $i=>$productcategory): ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="nowrap center">
                        <?php  echo JHtml::_('grid.id', $i, $productcategory->id);?>
                    </td>
                    <td class="nowrap center">
                        <?php  echo $productcategory->id;?>
                    </td>
                    <td class="has-context">
                        <a href="<?php echo JRoute::_('index.php?option=com_productcategory&task=productcategory.edit&id='.(int) $productcategory->id);?>">
                            <?php echo $this->escape(html_entity_decode($productcategory->bezeichnung));?>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>

        <input type="hidden" name="task" value="">
        <input type="hidden" name="boxchecked" value="0">
        <?php echo JHtml::_('form.token');?>
        <?php echo $this->pagination->getListFooter(); ?>


    </div>
</form>
