<?php
defined('_JEXEC') or die;
abstract class ModProductsHelper
{
    public static function getList(&$params)
    {
        if(JFactory::getApplication()->input->get('limitstart')) {

            $start = JFactory::getApplication()->input->get('limitstart');

        } else {

            $start = JFactory::getApplication()->input->get('start');

        }

        $prodCategoryId = (int) htmlentities($_GET['prodCat']);
        if(!empty($prodCategoryId)) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);


            $query->select($db->quoteName(array('produkte.id', 'produkte.bezeichnung','artikel_nr','menge', 'fresh', 'bio')), array('id', 'bezeichnung','artikel_nr','menge'));
            $query->from('produkte');
            $query->join('LEFT', 'produkte_produktgruppen on produkte.id = produkte_produktgruppen.produkt_id');
            $query->where('produkte_produktgruppen.produktgruppen_id = ' . $prodCategoryId . " and produkte.published=1");
            $db->setQuery($query, $start, 20);

            try {
                $results = $db->loadObjectList();
            } catch (\Symfony\Component\Yaml\Exception\RuntimeException $e) {
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

            foreach ($results as $row => $fields) {
                $results[$row] = new StdClass();
                $results[$row]->id = $fields->id;
                $results[$row]->bezeichnung = $fields->bezeichnung;
                $results[$row]->menge = $fields->menge;
                $results[$row]->bio = $fields->bio;
                $results[$row]->fresh = $fields->fresh;
                if(!empty($fields->menge)){
                    $menge = explode("/", $fields->menge);
                    if(preg_match("/([0-9]*)([a-z]*)/",$menge[0], $angaben)){
                        if ($angaben[1] >= 1000){
                            $results[$row]->tshirt_size = "xl";
                            $results[$row]->menge_nr = "100";
                        }elseif($angaben[1] < 1000 && $angaben[1] >=500){
                            $results[$row]->tshirt_size = "l";
                            $results[$row]->menge_nr = "75";
                        }elseif($angaben[1]> 160 && $angaben[1]<=500){
                            $results[$row]->tshirt_size = "m";
                            $results[$row]->menge_nr = "50";
                        }else{
                            $results[$row]->menge_nr = "25";
                            $results[$row]->tshirt_size = "s";
                        }
                    }
                }
                $string = mb_strtolower(html_entity_decode($fields->bezeichnung));
                $string = preg_replace('/\x{00e4}/u', "ae", $string);
                $string = preg_replace('/\x{00fc}/u', "ue", $string);
                $string = preg_replace('/\x{00f6}/u', "oe", $string);
                $string = preg_replace('/\x{00df}/u', "ss", $string);
                $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
                $string = preg_replace("/-/", "_", $string);
                $string = preg_replace('/\x{0020}/u', "_", $string);
                $string = preg_replace("/[^a-z0-9_]/", "", $string);
                $createdFileName = $string . "_" . str_pad($fields->artikel_nr, 4, 0, STR_PAD_LEFT);
                $results[$row]->filename = $createdFileName;
            }

            return $results;
        }else{
            return;
        }
    }
    public static function getNumberOfTotalResults(){
        /* select needs to be the same as in getList */
        $prodCategoryId = (int) htmlentities($_GET['prodCat']);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('count(*) as number');
        $query->from('produkte');
        $query->join('LEFT', 'produkte_produktgruppen on produkte.id = produkte_produktgruppen.produkt_id');
        $query->where('produkte_produktgruppen.produktgruppen_id = ' . $prodCategoryId . " and produkte.published=1");
        $db->setQuery($query);

        try {
            $results = $db->loadObjectList();
        } catch (\Symfony\Component\Yaml\Exception\RuntimeException $e) {
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
        return $results[0]->number;
    }

}