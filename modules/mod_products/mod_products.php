<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_products
 * @author Annika B.
 */

defined('_JEXEC') or die;

// Include the login functions only once
require_once __DIR__ . '/helper.php';

$products = ModProductsHelper::getList($params);
$totalResults = ModProductsHelper::getNumberOfTotalResults($params);

if(JFactory::getApplication()->input->get('limitstart')) {
    //Get this working with older Joomla versions
    $start = JFactory::getApplication()->input->get('limitstart');
 
} else {

    $start = JFactory::getApplication()->input->get('start');

}
$app = JFactory::getApplication();
$menu   = $app->getMenu();
$item = $menu->getActive();
$menuItem = $item->id;

jimport('joomla.html.pagination');
$pagination = new JPagination($totalResults, $start, 20);
require JModuleHelper::getLayoutPath('mod_products', $params->get('layout','default'));
