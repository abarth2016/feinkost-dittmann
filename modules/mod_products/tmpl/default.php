<?php
defined('_JEXEC') or die;
$page['productDetail'] = 56;
// $products run through

echo '<div class="content-products" style="background-image:url(\'images/icon/loading.gif\');background-repeat:no-repeat;background-position: center">';
if (!$products){
    //added animated gif
    echo "<div class=\"content-articles-headline\">Keine Produkte vorhanden</div>";
    echo "<div class=\"content-articles-description\">";
    echo 'In dieser Produktgruppe sind keine Produkte vorhanden.';
    echo "</div>";
}
foreach ($products as $product) {
    ?> 
    <div class="content-products-container">
        <a href="<?php echo JRoute::_('index.php?option=com_content&view=article&id='.$page['productDetail'].'&productId='.$product->id.'&Itemid='.$menuItem); ?>">
            <div class="content-products-status" style="z-index:1;">
                <?php if($product->bio == 1){ echo "<div style=\"background-image: url('images/content-products/banderole_gruen.png');background-repeat: no-repeat\" class=\"flag-bio\">bio</div>";}?>
                <?php if($product->fresh == 1){ echo "<div style=\"background-image: url('images/content-products/banderole_blau.png');background-repeat: no-repeat\" class=\"flag-fresh\">fresh</div>";}?>
<div class="content-products-headline"><?php echo $product->bezeichnung; ?></div>
            </div>
            <div class="content-products-status-preview">
                <?php
                $filename = "images/produktbilder/275_376/".$product->filename.".png";
                if(file_exists($filename)){
                    echo "<img style\"z-index:2;\" src=\"".$filename."\" alt=\"".htmlentities($product->bezeichnung)." ".$product->id."\" />";
                }else{
                    echo "<img style\"z-index:2;\" src=\"images/content-products-category/no_image.png\" alt=\"".$product->filename.".png\" />";
                }
                ?>
            </div>
            <img src="images/content-products-category/background.jpg" alt="" />
            <span class="products-quantity" style="z-index:0;background-color: rgba(246, 238, 238, 0.5);bottom: 0;height:78%;margin-right: -13px;position: absolute;left: 2px;width: 8px;">
                <span style="position:absolute;bottom:0;width:8px;height:<?php echo min($product->menge_nr,100);?>%;  background: red; /* For browsers that do not support gradients */
                    background: -webkit-linear-gradient(top,rgba(0,105,55,0.2),rgba(0,105,55,1)); /*Safari 5.1-6*/
                    background: -o-linear-gradient(bottom,rgba(0,105,55,0.2),rgba(0,105,55,1)); /*Opera 11.1-12*/
                    background: -moz-linear-gradient(bottom,rgba(0,105,55,0.2),rgba(0,105,55,1)); /*Fx 3.6-15*/
                    background: linear-gradient(to bottom, rgba(0,105,55,0.2), rgba(0,105,55,1)); /*Standard*/"></span>
                <div class="fuellbalken">
                    <span class="menge-s"><hr><span>S</span></span>
                    <span class="menge-m"><hr>M</span>
                    <span class="menge-l"><hr>L</span>
                    <span class="menge-xl"><hr>XL</span>
                </div>

            </span>
            <span style="z-index:0;font-family: 'Oswald';font-weight:lighter; sans-serif; color:rgba(4, 106, 56, 0.7);font-size:14px;position:absolute;left:12px;bottom:0px;
    "><?php echo $product->menge;?></span>
        </a>
    </div>
<?php }

?>
</div>
<div class="custom-pagination">
<?php
echo $pagination->getPagesLinks();
$pagination->getListFooter();
?>
</div>
<br style="clear:both">
