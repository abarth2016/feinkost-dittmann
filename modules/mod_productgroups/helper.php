<?php
/**
 * @author      abarth
 * @version     0.1
 * This helper will read all productgroups from database
 * and return an List of all productgroups
 *
 **/

// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

class ModProductgroupsHelper {

    public static function getList(  ) {

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('a.id as id, a.bezeichnung ');
        $query->from('welten as a');
        //$query->join('LEFT', 'produkte_welten as b  on a.id = b.welten_id');
        //$query->join('LEFT', 'produkte as c on b.produkte_id = c.id');
        $query->where('a.sichtbar=1');
        $db->setQuery((string) $query);
        $productgroups = $db->loadObjectList();
        foreach($productgroups as $productgroup){
            $string = mb_strtolower(html_entity_decode($productgroup->bezeichnung));
            $string = preg_replace('/\x{00e4}/u', "ae", $string);
            $string = preg_replace('/\x{00fc}/u', "ue", $string);
            $string = preg_replace('/\x{00f6}/u', "oe", $string);
            $string = preg_replace('/\x{00df}/u', "ss", $string);
            $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
            $string = preg_replace("/-/", "_", $string);
            $string = preg_replace('/\x{0020}/u', "_", $string);
            $string = preg_replace("/[^a-z0-9_]/","",$string);
            $createdFileName = $string. "_" . str_pad($productgroup->id, 3, 0, STR_PAD_LEFT);
            $productgroup->filename = $createdFileName;
        }
        return $productgroups;
    }

}

?>
