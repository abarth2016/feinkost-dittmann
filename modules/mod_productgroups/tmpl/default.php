<?php
defined('_JEXEC') or die;
//$page['productList'] = 59; // test
$page['productList'] = 61; //produktiv
// $productgroups run through 7 ADDED ANIMATED GIF
echo '<div class="content-products" style="background-image:url(\'images/icon/loading.gif\');background-position:50% 100px;background-repeat:no-repeat;background-position: center">';

foreach ($productgroups as $productgroup) {

    ?>
    
    <div class="content-productgroups-container">
        <a href="<?php echo JRoute::_('index.php?option=com_content&view=article&id='.$page['productList'].'&weltenId='.$productgroup->id.'&Itemid='.$menuArray[$productgroup->id]); ?>">
            <div class="content-products-status">
                <div class="content-products-headline"><?php echo $productgroup->bezeichnung; ?></div>
            </div>
            <div class="content-products-status-preview">
                <?php
                $filename = "images/mod_productgroups/".$productgroup->filename.".png";
                if(file_exists($filename)){
                    echo "<img src=\"".$filename."\" alt=\"".htmlentities($productgroup->bezeichnung)." ".$productgroup->id."\" />";

                }
                else {

                    echo "<img src=\"images/content-products/content-products-fallback-01.png\" />";
                }
                ?>
            </div>
            <img src="images/content-products-category/background.jpg" alt="" />

        </a>
    </div>
<?php } ?>
</div>
