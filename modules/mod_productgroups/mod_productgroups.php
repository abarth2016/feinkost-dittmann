<?php
// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

// helper.php aufrufen
require_once __DIR__ . '/helper.php';

// Parameter aufrufen und falls nicht definiert Werte zuweisen:
$productgroups = ModProductgroupsHelper::getList();


//get MenuItems to find out which Productgroup has which menu-highlight (id)
$app = JFactory::getApplication();
$menu   = $app->getMenu();
$items = $menu->getItems(array('parent_id'), array(122));

//Highlighting der Menüpunkte anhand der welten_id
// für Verlinkung erstellen
foreach($items as $menuItem){
    if(preg_match('/weltenId=([0-9]*)/', $menuItem->link, $matches)){
        $menuArray[$matches[1]] = $menuItem->id;
    }
}

$path = JModuleHelper::getLayoutPath('mod_productgroups');
require_once ($path);
?>