-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 13. Jul 2016 um 13:27
-- Server Version: 10.0.25-MariaDB-1~trusty
-- PHP-Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `welten` (
  `id` int(11) NOT NULL,
  `bezeichnung` varchar(255) DEFAULT NULL,
  `sichtbar` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `welten`
--

INSERT INTO `welten` (`id`, `bezeichnung`, `sichtbar`) VALUES
(1, 'Oliven', 1),
(2, 'Tapas / Antipasti', 1),
(3, 'Knoblauch', 1),
(4, 'Gewürze & Saucen', 1),
(5, 'Chips & Dips', 1),
(6, 'Caviar & Sardellen', 1),
(7, 'Essig & Öl', 1),
(8, 'Asiatische Feinkost', 1),
(9, 'Frische Säfte', 1),
(10, 'Pfefferonen', 1),
(11, 'Indische Spezialitäten', 1),
(12, 'Entdecker', 0),
(13, 'Kenner', 0),
(14, 'Helden', 0),
(15, 'Aufreisser', 0);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `welten`
--
ALTER TABLE `welten`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `welten`
--
ALTER TABLE `welten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
