<?php
// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

// helper.php aufrufen
require_once __DIR__ . '/helper.php';

// Parameter aufrufen und falls nicht definiert Werte zuweisen:

// default.php einbinden
// Als zweiter Parameter kann ein alternativer Dateiname
// übergeben werden:
// JModuleHelper::getLayoutPath('mod_lastusers', 'user')
// in diesem Fall wird statt der default.php die user.php aus
// dem tmpl Order geladen

$app = JFactory::getApplication();
$menu   = $app->getMenu();
$item = $menu->getActive();
$menuId = $item->id;

$recipeCategory = $_GET['recipeCategory'];
$recipelist = ModRecipeListHelper::getList($recipeCategory);

$path = JModuleHelper::getLayoutPath('mod_recipelist');
require_once ($path);
?>
