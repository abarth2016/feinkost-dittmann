<?php
defined('_JEXEC') or die;

// $recipelist run through
foreach ($recipelist as $recipe) {?>

<div class="content-articles">
	<div class="content-articles-preview">
    <?php 

    // Show Image of the recipe, filename is "rezept_003,jpg" for the recipe with the id 3
    $filename = 'images/rezeptbilder/618x390/'.$recipe->filename;
    if (file_exists($filename)){
        echo '<img src="'.$filename.'" alt=""></img> <span></span>';
    }else{
			echo '<img src="images/rezeptbilder/618x390/no_image.jpg" alt=""></img> <span></span>';
		}
    ?>
  </div>

	<span>

<div class="content-articles-headline"><?php echo $recipe->bezeichnung;?></div>

<div class="content-articles-description"><?php
if(strlen($recipe->zubereitung) > 450){

    $recipe->zubereitung = preg_replace('/\s+?(\S+)?$/', '', substr($recipe->zubereitung, 0, 450))."...";

    echo $recipe->zubereitung;

}else{
	echo $recipe->zubereitung;
	}
?></div>

<div class="content-articles-button"><a lang="de" href="index.php?option=com_content&view=article&id=57&Itemid=<?php echo $menuId;?>&lang=de&recipeId=<?php echo $recipe->id;?>">Weiter</a></div>

</span>
</div>

<?php } ?>
