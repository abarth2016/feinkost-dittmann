<?php
/**
 * @author      abarth
 * @version     0.1
 * This helper will read all recipes from database with the category given in the url (recipeCategory)
 * and return an List of all recipes to
 * create an recipelist
 *
 **/

// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

class ModRecipelistHelper {

    public static function getList( $recipeCategory ) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('recipes');
        if (isset($recipeCategory)) {
            $query->join('LEFT', "rezepte_kategorie on recipes.id = rezepte_kategorie.rezepte_id");
            $query->where('rezepte_kategorie.kategorie_id = ' . htmlentities($recipeCategory) . '');
        }
        $db->setQuery((string) $query);
        $recipes = $db->loadObjectList();

        foreach($recipes as $key => $value){
            mb_internal_encoding("UTF-8");
            $headline = $recipes[$key]->bezeichnung;
            $string = html_entity_decode($headline);
            $string = mb_strtolower($string);
            $string = preg_replace('/\x{00e4}/u', "ae", $string);
            $string = preg_replace('/\x{00fc}/u', "ue", $string);
            $string = preg_replace('/\x{00f6}/u', "oe", $string);
            $string = preg_replace('/\x{00df}/u', "ss", $string);
            $string = preg_replace('/\r\n/', "_", $string);
            $string = preg_replace('/\n/', "_", $string);
            $string = preg_replace('/\r/', "_", $string);
            $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
            $string = preg_replace("/-/", "_", $string);
            $string = preg_replace('/\x{0020}/u', "_", $string);
            $createdFileName = preg_replace("/[^a-z0-9_]/","",$string);
            $recipes[$key]->filename = $createdFileName."_" . str_pad($recipes[$key]->id, 3, 0) . "." . "jpg";
            //echo $createdFileName."\n";
        }
        return $recipes;
    }

}

?>
