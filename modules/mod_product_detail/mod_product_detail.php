<?php
// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

// helper.php aufrufen
require_once __DIR__ . '/helper.php';

$productId = $_GET['productId'];
$product = ModProductdetailHelper::getList($productId);
$params = new JRegistry($module->params);

$path = JModuleHelper::getLayoutPath('mod_product_detail');
require_once ($path);
?>
