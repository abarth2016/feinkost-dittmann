<?php
defined('_JEXEC') or die;
if ($product){
?>

<div class="content-articles">
    <div class="content-articles-preview product-detail-left-column">
        <div class="product-container">
            <?php

            $filename = 'images/produktbilder/275_376/'.$product->filename;

            if (file_exists($filename)){
                echo '<div class="product-centerer"><img src="'.$filename.'" alt="" /></div>';
            }else{
                echo '<div class="product-centerer"><img src="images/content-products-category/no_image.png" alt="" /></div>';
            }
            ?>
            <img src="images/content-products-category/background.jpg" alt="" />
            <span class="products-quantity" style="z-index:0;background-color: rgba(246, 238, 238, 0.5);bottom: 0;height:78%;margin-right: -13px;position: absolute;left: 2px;width: 8px;">
                <span style="position:absolute;bottom:0;width:8px;height:<?php echo min($product->menge_nr,100);?>%;  background: red; /* For browsers that do not support gradients */
                    background: -webkit-linear-gradient(top,rgba(0,105,55,0.2),rgba(0,105,55,1)); /*Safari 5.1-6*/
                    background: -o-linear-gradient(bottom,rgba(0,105,55,0.2),rgba(0,105,55,1)); /*Opera 11.1-12*/
                    background: -moz-linear-gradient(bottom,rgba(0,105,55,0.2),rgba(0,105,55,1)); /*Fx 3.6-15*/
                    background: linear-gradient(to bottom, rgba(0,105,55,0.2), rgba(0,105,55,1)); /*Standard*/"></span>
                <div class="fuellbalken">
                    <span class="menge-s"><hr><span>S</span></span>
                    <span class="menge-m"><hr>M</span>
                    <span class="menge-l"><hr>L</span>
                    <span class="menge-xl"><hr>XL</span>
                </div>

            </span>
        </div>
        <div class="info-span">
<div class="content-articles-information">
    <div class="content-articles-information-border">
        <div class="content-details-headline">Zusatzinformationen</div>
        <div id="content-info" class="product-info-inner">
            <div class="content-details"><span>Marke:</span><span class="info-content"><?php echo $product->marke;?></span></div>
            <div class="content-details"><span>Artikel Nr.:</span><span class="info-content"><?php echo $product->art_nr;?></span></div>
            <div class="content-details"><span>Menge:</span><span class="info-content"><?php echo $product->menge;?></span></div>
            <div class="content-details"><span>Verpackung:</span><span class="info-content"><?php echo $product->verpackung;?></span></div>
            <div class="content-details"><span>Anzahl VE:</span><span class="info-content"><?php echo $product->verpackungseinheit;?></span></div>
            <div class="content-details"><span>EAN:</span><span class="info-content"><?php echo $product->ean;?></span></div>
        </div>
    </div>
</div>
</div></div>
    <span>
<div class="content-articles-headline"><?php echo str_replace("ß", "ss", $product->produktbezeichnung);?></div>
<div class="content-articles-description"><?php echo $product->beschreibung;?></div>
<div class="content-articles-button">
        <?php if (isset($product->shop_produkt_id)){ ?>
        <a href="http://www.feinkost-dittmann-shop.de/index.php/catalog/product/view/id/<?php echo $product->shop_produkt_id; ?>">Zum Onlineshop</a>
        <?php }?>
</div>
</span></div>
<?php
}else{ ?>
    <div>
        Es wurde keine Product-Id übergeben
    </div>
<?php } ?>
