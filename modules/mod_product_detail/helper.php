<?php
/**
 * @author      abarth
 * @version     0.1
 * get a product from database by id to show in product detail
 **/

// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

class ModProductdetailHelper {

    public static function getList( $productId  ) {
        $productId = htmlentities($productId);

        if (!empty($productId)) {
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select('*, marken.bezeichnung as marke, verpackungen.bezeichnung as verpackung, produkte.bezeichnung as produktbezeichnung,produkte.id as produkte_id, produkte_artikel.magento_id as shop_produkt_id, produkte.artikel_nr as art_nr');
            $query->from('produkte');
            $query->join('LEFT', 'marken on marken.id = produkte.marken_id');
            $query->join('LEFT', 'verpackungen on verpackungen.id = produkte.verpackungen_id');
            $query->join('LEFT', 'produkte_artikel on produkte_artikel.artikel_nr = produkte.artikel_nr');
            $query->where('produkte.id = ' . $productId . " and published=1");

            $db->setQuery((string)$query);
            $product = $db->loadObjectList();

            $string = mb_strtolower(html_entity_decode($product[0]->produktbezeichnung));
            $string = preg_replace('/\x{00e4}/u', "ae", $string);
            $string = preg_replace('/\x{00fc}/u', "ue", $string);
            $string = preg_replace('/\x{00f6}/u', "oe", $string);
            $string = preg_replace('/\x{00df}/u', "ss", $string);
            $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
            $string = preg_replace("/-/", "_", $string);
            $string = preg_replace('/\x{0020}/u', "_", $string);
            $string = preg_replace("/[^a-z0-9_]/", "", $string);
            $createdFileName = $string . "_" . str_pad($product[0]->art_nr, 4, 0, STR_PAD_LEFT);
            $product[0]->filename = $createdFileName.".png";
            if(!empty($product[0]->menge)){
                $menge = explode("/", $product[0]->menge);
                if(preg_match("/([0-9]*)([a-z]*)/",$menge[0], $angaben)){
                    if ($angaben[1] >= 1000){
                        $product[0]->menge_nr = "100";
                    }elseif($angaben[1] < 1000 && $angaben[1] >=500){
                        $product[0]->menge_nr = "75";
                    }elseif($angaben[1]> 160 && $angaben[1]<=500){
                        $product[0]->menge_nr = "50";
                    }else{
                        $product[0]->menge_nr = "25";
                    }
                }
            }
            return $product[0];
        }else{
            return false;
        }
    }

}

?>
