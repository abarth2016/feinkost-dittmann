<?php
defined('_JEXEC') or die;
$page['productDetail'] = '56';
// $productgroups run through
?>
<!-- SLIDER -->
<div class="content-slider-background">
	<div class="content-slider">

		<div class="content-slider-media">

			<div class="content-slider-media-container">
            <?php foreach ($products as $product) {?>
                <div class="content-slider-media-status">
                    <a href="<?php echo JRoute::_('index.php?option=com_content&view=article&id='.$page['productDetail'].'&Itemid=173&productId='.$product->produkt_id); ?>">
                        <div class="content-slider-media-status-container">
                            <?php
                            $filename = 'images/produktbilder/275_376/'.$product->filename.".png";
                            if (file_exists($filename)){
                                echo '<img src="'.$filename.'" alt=""></img>';
                            }else{
                                echo "<img src=\"images/content-products/content-products-fallback-01.png\" />";
                            }
                            ?>
                        </div>
                        <img src="images/content-slider/content-slider-01.jpg" alt="" />
                        <div class="content-slider-media-status-headline"><span><?php echo $product->produkte_bezeichnung;?></span></div>
                    </a>
                </div>
            <?php } ?>
			</div>
		</div>
		<div class="content-slider-navigation">
			<div id="back" style="left: -24px;" class="content-slider-navigation-button"><img src="images/content-slider/content-slider-button-01.png" alt="" /></div>
			<div id="next" style="right: -24px;" class="content-slider-navigation-button"><img src="images/content-slider/content-slider-button-02.png" alt="" /></div>
		</div>
	</div>
</div>

