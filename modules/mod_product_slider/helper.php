<?php
/**
 * @author      abarth
 * @version     0.1
 * This helper will read all productgroups from database
 * and return an List of all productgroups
 *
 **/

// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

class ModProductsliderHelper {

    public static function getList( $prodSliderId = null, $brandSliderId=null) {
        $prodSliderId = htmlentities($prodSliderId);
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*, produkte.id as produkt_id, produkte.bezeichnung as produkte_bezeichnung');
        $query->from('produkte');
        if (isset($prodSliderId) && !empty($prodSliderId)) {
            $query->join('LEFT', 'produkte_produktgruppen on produkte.id = produkte_produktgruppen.produkt_id');
            $query->join('LEFT', 'welten_produktgruppen on welten_produktgruppen.produktgruppen_id = produkte_produktgruppen.produktgruppen_id');
            $query->where('welten_produktgruppen.welten_id = ' . $prodSliderId . " and produkte.published=1");
            $query->order('FLOOR(RAND()*1000)');
        }elseif (isset($brandSliderId) && !empty($brandSliderId)) {
            $query->join('LEFT', 'marken on produkte.marken_id = marken.id');
            $query->where('marken.id = ' . $brandSliderId . " and produkte.published=1");
            $query->order("produkt_id desc");
        }else{
            $query->where('id <= 800');
            $query->order('FLOOR(RAND()*1000)');
        }
        $query->setLimit(15);
        $db->setQuery((string) $query);
        $products = $db->loadObjectList();

        foreach($products as $product){
            $string = mb_strtolower(html_entity_decode($product->produkte_bezeichnung));
            $string = preg_replace('/\x{00e4}/u', "ae", $string);
            $string = preg_replace('/\x{00fc}/u', "ue", $string);
            $string = preg_replace('/\x{00f6}/u', "oe", $string);
            $string = preg_replace('/\x{00df}/u', "ss", $string);
            $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
            $string = preg_replace("/-/", "_", $string);
            $string = preg_replace('/\x{0020}/u', "_", $string);
            $string = preg_replace("/[^a-z0-9_]/","",$string);
            $createdFileName = $string. "_" . str_pad($product->artikel_nr, 4, 0, STR_PAD_LEFT);
            $product->filename = $createdFileName;
        }

        return $products;
    }

}

?>
