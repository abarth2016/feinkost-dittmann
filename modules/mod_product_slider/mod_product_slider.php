<?php
// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

// helper.php aufrufen
require_once __DIR__ . '/helper.php';


$prodSliderId = isset($_GET['prodSliderId'])?$_GET['prodSliderId']:'';
$brandSliderId = isset($_GET['brandSliderId'])?$_GET['brandSliderId']:'';

$products = ModProductsliderHelper::getList($prodSliderId, $brandSliderId);
$params = new JRegistry($module->params);

$path = JModuleHelper::getLayoutPath('mod_product_slider');
require_once ($path);
?>
