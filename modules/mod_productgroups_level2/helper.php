<?php
/**
 * @author      abarth
 * @version     0.1
 * This helper will read all productgroups from database
 * and return an List of all productgroups
 *
 **/

// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

class ModProductgroupsLevel2Helper {

    public static function getList( $params) {
        $weltenId = (int) htmlentities($_GET['weltenId']);

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*, produktgruppen.bezeichnung as produktgruppenbezeichnung');
        $query->from('produktgruppen');
        $query->join('LEFT', 'welten_produktgruppen  on produktgruppen.id = welten_produktgruppen.produktgruppen_id');
        $query->where('welten_produktgruppen.welten_id = '.$weltenId);
        $db->setQuery((string) $query);
        $productgroups_level2 = $db->loadObjectList();
        foreach($productgroups_level2 as $productgroup){
            $string = mb_strtolower(html_entity_decode($productgroup->produktgruppenbezeichnung));
            $string = preg_replace('/\x{00e4}/u', "ae", $string);
            $string = preg_replace('/\x{00fc}/u', "ue", $string);
            $string = preg_replace('/\x{00f6}/u', "oe", $string);
            $string = preg_replace('/\x{00df}/u', "ss", $string);
            $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
            $string = preg_replace("/-/", "_", $string);
            $string = preg_replace('/\x{0020}/u', "_", $string);
            $string = preg_replace("/[^a-z0-9_]/","",$string);
            $createdFileName = $string. "_" . str_pad($productgroup->produktgruppen_id, 3, 0, STR_PAD_LEFT);
            $productgroup->filename = $createdFileName;
        }
        return $productgroups_level2;
    }

}

?>
