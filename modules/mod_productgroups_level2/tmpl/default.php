<?php
defined('_JEXEC') or die;
//$page['productlist'] = 57; //test
$page['productlist'] = 60; //produktiv 
// $products run through


echo '<div class="content-products">';
if (!$productgroups_level2){
    if (!$products){
        //added animated gif
        echo "<div class=\"content-articles-headline\">Diese Produktgruppe existiert nicht</div>";
        echo "<div class=\"content-articles-description\">";
        echo 'Die angesprochene Produktgruppe ist leer oder existiert nicht. Bitte überprüfen Sie den Link zu dieser Seite.';
        echo "</div>";
    }}

foreach ($productgroups_level2 as $product) {
    ?>
    <div class="content-productgroups-container">
        <a href="<?php echo JRoute::_('index.php?option=com_content&view=article&id='.$page['productlist'].'&prodCat='.$product->produktgruppen_id.'&Itemid='.$menuItem); ?>">
            <div class="content-products-status" style="z-index:1;">
                <div class="content-products-headline"><?php echo $product->produktgruppenbezeichnung; ?></div>
            </div>
            <div class="content-products-status-preview">
                <?php
                $filename = "images/produktgruppen/275_376/".$product->filename.".png";
                if(file_exists($filename)){
                    echo "<img style\"z-index:2;\" src=\"".$filename."\" alt=\"".htmlentities($product->produktgruppenbezeichnung)." ".$product->produktgruppen_id."\" />";
                }else{
                    echo "<img style\"z-index:2;\" src=\"images/content-products-category/no_image.png\" alt=\"".$filename."\" />";
                }
                ?>
            </div>
            <img src="images/content-products-category/background.jpg" alt="" />
        </a>
    </div>
<?php }
?>
</div>
<div class="custom-pagination">

</div>
<br style="clear:both">
