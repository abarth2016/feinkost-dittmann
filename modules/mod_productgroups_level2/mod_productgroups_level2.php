<?php
// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

// helper.php aufrufen
require_once __DIR__ . '/helper.php';

// Parameter aufrufen und falls nicht definiert Werte zuweisen:
$productgroups_level2 = ModProductgroupsLevel2Helper::getList($params);

$app = JFactory::getApplication();
$menu   = $app->getMenu();
$item = $menu->getActive();
$menuItem = $item->id;
 
$path = JModuleHelper::getLayoutPath('mod_productgroups_level2');
require_once ($path);
?>