<?php
/**
 * @author      Joomla!-Nafu
 * @authorurl   http://www.joomla-nafu.de
 * @version     0.1
 * @license     GNU General Public License, http://www.gnu.org/licenses/gpl-2.0.html
 *
 * JNafu! - Joomla! 1.5.x Modul, dass die letzten X eingeloggten User auflistet
 * Copyright (C) 2009 - Joomla!-Nafu
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

class ModRecipesHelper {

    public static function getRecipes($recipeId) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        if (isset($recipeId) && !empty($recipeId)) {
            $query->select('*');
            $query->from('recipes a');
            $query->where('a.id=' . htmlentities($recipeId));
            $db->setQuery((string)$query);
            $recipe = $db->loadObject();

            $queryIngredients = $db->getQuery(true);
            $queryIngredients->select('*');
            $queryIngredients->from("ingredients");
            $queryIngredients->join("LEFT", "produkte_artikel on ingredients.artikel_nr = produkte_artikel.artikel_nr");
            $queryIngredients->where("recipe_id = " . htmlentities((int)$recipeId));
            $queryIngredients->order("ingredients.id ASC");
            $db->setQuery((string)$queryIngredients);
            if (!$ingredients = $db->loadAssocList()){
                $ingredients = array();
            }
            foreach($ingredients as $ingredient) {
                if (!empty($ingredient['magento_id']) && $ingredient['artikel_nr'] != 0) {
                    $productId[] = array('id' => $ingredient['magento_id'], 'qty' => 1);

                }
            }
            $recipe->jsonString = json_encode($productId);
            $recipe->ingredients = $ingredients;

            $string = mb_strtolower(html_entity_decode($recipe->bezeichnung));

            $string = preg_replace('/\x{00e4}/u', "ae", $string);
            $string = preg_replace('/\x{00fc}/u', "ue", $string);
            $string = preg_replace('/\x{00f6}/u', "oe", $string);
            $string = preg_replace('/\x{00df}/u', "ss", $string);
            $string = preg_replace('/\r\n/', "_", $string);
            $string = preg_replace('/\n/', "_", $string);
            $string = preg_replace('/\r/', "_", $string);
            $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
            $string = preg_replace("/-/", "_", $string);
            $string = preg_replace('/\x{0020}/u', "_", $string);
            $string = preg_replace("/[^a-z0-9_]/", "", $string);
            $createdFileName = $string . "_" . str_pad($recipe->id, 3, 0);
            $recipe->filename = $createdFileName;
        }

        return $recipe;

    }
}

?>
