<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_random_image
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<?php
if (isset($recipe) && !empty($recipe)){
    // $users durchlaufen und jeweils jeden User ausgeben

        $positionOfFirstSpace = strpos ( trim($recipe->bezeichnung) , ' ');
        function decToFraction($floatingNumber) {
            $decimal = fmod($floatingNumber,1);
            $leastCommonDenom = 16;
            $denominators = array (2, 3, 4, 8, 16 );
            $roundedDecimal = round ( $decimal * $leastCommonDenom ) / $leastCommonDenom;

            if ($roundedDecimal == 0)
                return (int) $floatingNumber;

            foreach ( $denominators as $d ) {
                if ($roundedDecimal * $d == floor ( $roundedDecimal * $d )) {
                    $denom = $d;
                    break;
                }
            }
            return ($roundedDecimal * $denom) == 0? "" : ($roundedDecimal * $denom) . "/" . $denom;
        }

?>

<div id="header-preview">

    <?php

    $filename = 'images/rezeptbilder/1430x425/'.$recipe->filename.".jpg";

    if (file_exists($filename)){
        echo '<img src="'.$filename.'" alt="">';
    }

    else {


    }

    $headlineInParts = mb_split ( '/\r\n|\n|\r/' , $recipe->bezeichnung );

    ?>

    <div id="header-preview-status">
        <div class="header-preview-status-headline"><img src="images/navigation-recipes/navigation-recipes-icon.png" alt="" /><span><?php echo $headlineInParts[0];?></span>
            <div class="header-preview-status-description"><?php echo $headlineInParts[1]; ?></div>
        </div>
    </div>
</div><p>
<div class="content-distance background-green">
<div class="content-distance-border border-yellow"></div>
</div>
<!-- CONTENT -->

    <div class="content-articles">

        <div class="content-articles-preview">

            <?php
            // Show Image of the recipe, filename is "rezept_003,jpg" for the recipe with the id 3
            $filename = 'images/rezeptbilder/618x390/'.$recipe->filename.'.jpg';
            if (file_exists($filename)){
                echo '<img src="'.$filename.'" alt="">';
            }
            ?>


            <span>

    <div class="content-articles-information">

        <div class="content-articles-information-border">

            <div class="content-ingredients-headline">Zutaten</div>

    <div id="content">

    <?php

    foreach ($recipe->ingredients as $ingredient) {

        $measure_units = array(
            0 => '',
            1 => array('g', 'g'),
            2 => array('ml', 'ml'),
            3 => array('EL', 'EL'),
            4 => array('Glas', 'Gläser'),
            5 => array('Msp.', 'Msp'),
            6 => array('Prise', 'Prisen'),
            7 => array('TL', 'TL'),
            8 => array('kg', 'kg'),
            9 => array('Stiel', 'Stiele'),
            10 => array('Bund', 'Bund'),
            11 => array('Scheibe', 'Scheiben'),
            12 => array('Kopf', 'Köpfe'),
            13 => array('Zehe', 'Zehen'),
            14 => array('Würfel', 'Würfel'),
            15 => array('Becher', 'Becher'),
            16 => array('Stück', 'Stück'),
            17 => array('Beet', 'Beete'),
            18 => array('Dose', 'Dosen'),
            19 => array('Pkt.', 'Pkt.')
        );

        $addSpace = true;

        switch ($ingredient['measuringunit']) {
            case 1:
            case 2:
            case 8:
                $addSpace = false;
                break;
            default:
                $addSpace = true;
        }

        $quantity = decToFraction($ingredient['quantity']);

        if(isset($ingredient['magento_id']) && !empty($ingredient['magento_id'])) {

?>

        <div class="content-ingredients">

            <?php

            if($quantity != 0) {

                echo "<span>".decToFraction($ingredient['quantity'])."</span>";

            }

            echo $addSpace == true && ($quantity != 0) ? "&nbsp;":"";

            echo ($quantity<1)?$measure_units[($ingredient['measuringunit'])][0]:$measure_units[($ingredient['measuringunit'])][1];

            ?>

            <div id="product-id-<?php echo $ingredient['magento_id'];?>" class="product-id">

                <a href='http://www.feinkost-dittmann-shop.de/index.php/catalog/product/view/id/<?php echo $ingredient['magento_id'];?>' target='_blank'>

                    <?php

                    echo $ingredient['bezeichnung'];

                    ?>

                    <img src="images/content-ingredients/content-ingredients-icon-01.png" />

                </a>

            </div>

        </div>

            <?php

        }

        else {

?>

        <div class="content-ingredients">

            <?php

            if($quantity != 0) {

                echo "<span>".decToFraction($ingredient['quantity'])."</span>";

            }

            echo $addSpace == true && ($quantity != 0) ? "&nbsp;":"";

            echo ($quantity<1)?$measure_units[($ingredient['measuringunit'])][0]:$measure_units[($ingredient['measuringunit'])][1];

            if($ingredient['measuringunit'] != 0) {

                echo "&nbsp;".$ingredient['bezeichnung'];

            }

            else {

                echo $ingredient['bezeichnung'];

            }

            ?>

        </div>

            <?php

        }

    }

        ?>

    </div>

        </div>

    </div>

</span>

            <div class="content-ingredients-status">

                <div class="content-ingredients-status-border">

                    <div class="content-ingredients-status-container">

                        <div id="number"><span><input id="multiplicator" value="4" maxlength="3" type="text"></span>&nbsp;<b>Portion(en)</b></div>
                        <div id="content-ingredients-notification">Kennen Sie schon <a href="http://gourmet-factory.feinkost-dittmann.de" target="_blank">Gourmet Factory</a>?</div>
                        <div id="content-ingredients-button-convert"><span>Umrechnen</span></div>
                        <!-- <div id="content-ingredients-button-cart"><span>In den Warenkorb</span></div> -->
<?php if(!empty($recipe->jsonString) && $recipe->jsonString != "null"){?>
            <form class="content-ingredients-cart-form" action="http://localhost/feinkostdittmann/recipes/index/addToCart" method="post">
                <input type="hidden" id="jsonString" name="products" value='<?php echo $recipe->jsonString;?>'>
                <input type="submit" id="warenkob-submit" value="In den Warenkorb">
            </form>
<?php } ?>
                    </div>

                </div>

            </div>

        </div>

    <span>

    <div class="content-articles-headline">Zubereitung</div>

    <div class="content-articles-description"><?php echo nl2br($recipe->zubereitung);?></div>

</span>

    </div>

<?php } ?>