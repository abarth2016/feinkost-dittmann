<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_random_image
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<?php
// $users durchlaufen und jeweils jeden User ausgeben
foreach ($recipes as $recipe) {
$ingredients = explode("\n",$recipe->zutaten);
$positionOfFirstSpace = strpos ( $recipe->bezeichnung , ' ');

?>
<!-- CONTENT -->
            <span>
    <div class="content-articles-information">
        <div class="content-articles-information-border">
            <div class="content-ingredients-headline">Zutaten</div>
            <div id="content" style="height: 168px;">
                <?php foreach ($ingredients as $ingredient){
                    $pattern = "/([0-9\/\-]*)(.*)/";
                    $subject = $ingredient;
                    preg_match ( $pattern , $subject, $partsOfIngredient );
                    //var_dump($matches);
                    ?>



                    <div class="content-ingredients" style="position: absolute; display: block; left: 0px; top: 36px;"><span><?php echo isset($partsOfIngredient[1])?$partsOfIngredient[1].'&nbsp;':'';?></span><?php echo isset($partsOfIngredient[2])?$partsOfIngredient[2]:'';?></div>



                    <?php
                    /*
                      <div class="content-ingredients" style="position: absolute; display: block; left: 249px; top: 108px;"><span>1/4</span>&nbsp;Fl.
                        <div id="product-id-104" class="product-id"><a href="http://www.feinkost-dittmann-shop.de/index.php/catalog/product/view/id/104" target="_blank">Ponti Balsamico Creme<img src="images/content-ingredients/content-ingredients-icon-01.png" alt=""></a></div>
                        <!--
                    <div class='product-category'>
                    <a href='http://www.feinkost-dittmann-shop.de/index.php/catalog/category/view/id/25' target='_blank'>Kategorie im Onlineshop</a>
                    </div>
                      --></div>
                    */


                    ?>
                <?php } ?>
            </div>
        </div>
    </div>
</span>
    <?php } ?>
