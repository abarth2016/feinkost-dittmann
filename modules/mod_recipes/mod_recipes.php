<?php
/**
 * @author      Joomla!-Nafu
 * @authorurl   http://www.joomla-nafu.de
 * @version     0.1
 * @license     GNU General Public License, http://www.gnu.org/licenses/gpl-2.0.html
 *
 * JNafu! - Joomla! 1.5.x Modul, dass die letzten X eingeloggten User auflistet
 * Copyright (C) 2009 - Joomla!-Nafu
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

// Verhindern, dass die Datei direkt aufgerufen wird
defined('_JEXEC') or die('Restricted access');

// helper.php aufrufen
require_once __DIR__ . '/helper.php';

// Parameter aufrufen und falls nicht definiert Werte zuweisen:

// Wenn der Username angezeigt werden soll, dann ist der Wert 1,
// beim "echten" Namen ist der Wert 0, falls der Wert nicht
// vorhanden ist, wird er auf 1 gesetzt


// default.php einbinden
// Als zweiter Parameter kann ein alternativer Dateiname
// übergeben werden:
// JModuleHelper::getLayoutPath('mod_lastusers', 'user')
// in diesem Fall wird statt der default.php die user.php aus
// dem tmpl Order geladen

$recipeId = $_GET['recipeId'];
$recipe = ModRecipesHelper::getRecipes($recipeId);
$params = new JRegistry($module->params);
$path = JModuleHelper::getLayoutPath('mod_recipes');

require_once ($path);
?>
