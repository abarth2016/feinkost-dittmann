<?php 

	defined('_JEXEC') or die;

	$this->setGenerator(null);

	$initDocumentData = JFactory::getDocument();

	$initDocumentData->addScript($this->baseurl."/templates/".$this->template."/scripts/jquery-functions-1.0.0.js");

	$initDocumentData->addScript($this->baseurl."/templates/".$this->template."/scripts/contentSlider-1.0.0.js");

    $initDocumentData->addScript($this->baseurl."/templates/".$this->template."/scripts/popUp.js");
    $initDocumentData->addScript($this->baseurl."/templates/".$this->template."/scripts/contact.js");
/* test*/

	$initDocumentData->addScript($this->baseurl."/templates/".$this->template."/scripts/jquery-ui-1.10.0.js");

	$initDocumentData->addStyleSheet($this->baseurl."/templates/".$this->template."/css/template.css");

	$initAppData = JFactory::getApplication();

	$initMenuData = $initAppData->getMenu()->getActive();

	$initPageClass = "";

	if(is_object($initMenuData)) {

    	$initPageClass = $initMenuData->params->get('pageclass_sfx');

	}

	if($initPageClass == "") {

		$initPageClass = "null";

	}

	$this->_script = $this->_scripts = array();

?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

	<head>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />



		<script type="text/javascript" src="<?php echo $this->baseurl."/templates/".$this->template ?>/scripts/jquery-2.2.0.js"></script>
        <script type="text/javascript" src="<?php echo $this->baseurl."/templates/".$this->template ?>/scripts/contentSlider-1.0.0.js"></script>


        <script type="text/javascript" src="<?php echo $this->baseurl."/templates/".$this->template ?>/scripts/jquery-ui-1.10.0.js"></script>
        <script type="text/javascript" src="<?php echo $this->baseurl."/modules/mod_jq-dailypop/js/js.js";?>"></script>
        <script type="text/javascript" src="<?php echo $this->baseurl."/templates/".$this->template ?>/scripts/popUp.js"></script>


        <script type="text/javascript" src="<?php echo $this->baseurl."/templates/".$this->template ?>/scripts/slidegridpro.min.js"></script>

		<script type="text/javascript" src="<?php echo $this->baseurl."/templates/".$this->template ?>/scripts/jquery-functions-1.0.0.js"></script>
        <script type="text/javascript" src="<?php echo $this->baseurl."/templates/".$this->template ?>/scripts/contact.js"></script>
        <jdoc:include type="head" />
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBP9MjHwbEFyBTtcD3Dpq7BJgAUI0pbogs&callback=initialize" type="text/javascript"></script>
		<script type="text/javascript">
            <!--

			function initialize() {

                var flags_001 = [
                    ['Diez', 50.3750723, 8.0256215, 1]
                ];

                var flags_002 = [
                    ['Taunusstein', 50.145986, 8.147932, 1]
                ];

                var flags_003 = [
                    ['Villmar', 50.400387, 8.196364, 1]
                ];

                var flags_004 = [
                    ['Berriatua', 43.318018, -2.463420, 1]
                ];

                var flags_005 = [
                    ['Alexandria', 40.639979, 22.443795, 1]
                ];

                var flags_006 = [
                    ['Turgutlu', 38.504044, 27.688656, 1]
                ];

                if ($("#map-canvas-001").length === 1) {

                    if (document.getElementById("map-canvas-001")) {

                        var mapOptions_001 = {zoom: 11, center: new google.maps.LatLng(50.3670723, 8.0502215)};

                        var map_001 = new google.maps.Map(document.getElementById('map-canvas-001'), mapOptions_001);

                        setMarkers(map_001, flags_001);

                    }

                    if (document.getElementById("map-canvas-002")) {

                        var mapOptions_002 = {zoom: 11, center: new google.maps.LatLng(50.134986, 8.147932)};

                        var map_002 = new google.maps.Map(document.getElementById('map-canvas-002'), mapOptions_002);

                        setMarkers(map_002, flags_002);

                    }

                    if (document.getElementById("map-canvas-003")) {

                        var mapOptions_003 = {zoom: 11, center: new google.maps.LatLng(50.400387, 8.196364)};

                        var map_003 = new google.maps.Map(document.getElementById('map-canvas-003'), mapOptions_003);

                        setMarkers(map_003, flags_003);

                    }

                    if (document.getElementById("map-canvas-004")) {

                        var mapOptions_004 = {zoom: 11, center: new google.maps.LatLng(43.321088, -2.454420)};

                        var map_004 = new google.maps.Map(document.getElementById('map-canvas-004'), mapOptions_004);

                        setMarkers(map_004, flags_004);

                    }

                    if (document.getElementById("map-canvas-005")) {

                        var mapOptions_005 = {zoom: 11, center: new google.maps.LatLng(40.639979, 22.443795)};

                        var map_005 = new google.maps.Map(document.getElementById('map-canvas-005'), mapOptions_005);

                        setMarkers(map_005, flags_005);

                    }

                    if (document.getElementById("map-canvas-006")) {

                        var mapOptions_006 = {zoom: 11, center: new google.maps.LatLng(38.504044, 27.688656)};

                        var map_006 = new google.maps.Map(document.getElementById('map-canvas-006'), mapOptions_006);

                        setMarkers(map_006, flags_006);

                    }

                }


                function setMarkers(map, locations) {

                    var image = {
                        url: "images/content-google-maps/content-google-maps-icon-01.png",
                        size: new google.maps.Size(64, 62),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(58, 62)
                    };

                    var shape = {coords: [1, 1, 1, 20, 18, 20, 18, 1], type: 'poly'};

                    for (var i = 0; i < locations.length; i++) {

                        var flag = locations[i];
                        var myLatLng = new google.maps.LatLng(flag[1], flag[2]);
                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            icon: image,
                            shape: shape,
                            title: flag[0],
                            zIndex: flag[3]
                        });

                    }

                }

                /* google.maps.event.addDomListener(window, 'load', initialize);*/
            }
//-->
		</script>

		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet" type="text/css">

		<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet" type="text/css">

		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet" type="text/css">

		<link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet" type="text/css">

	</head>

	<body>

  		<jdoc:include type="component" />

	</body>

</html>