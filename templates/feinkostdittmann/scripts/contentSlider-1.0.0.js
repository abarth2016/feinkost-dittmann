jQuery(document).ready(function ($) {
    var visibleProducts = 5;

    $(window).load(function () {
        initializeContentSlider();
    });

    $(window).resize(function () {
        initializeContentSlider();
    });

    var init_slider_id = 1;

    initializeContentSlider = function () {
        windowSize = $(window).width();
        var numberOfChildren = $(".content-slider-media-container").children().size();
        var contentSliderWidth = $(".content-slider").width();

        if (numberOfChildren < 2) {
            if (windowSize >= 768) {
                init_slider_width = 50;
                visibleProducts = 2
            } else {
                init_slider_width = 100;
                visibleProducts = 1
            }
        } else {
            if (windowSize < 480) {
                init_slider_width = 100;
                visibleProducts = 1
            } else if (windowSize < 768) {
                init_slider_width = 50;
                visibleProducts = 2
            } else if (windowSize < 992) {
                init_slider_width = 25;
                visibleProducts = 4
            } else {
                init_slider_width = 20;
                visibleProducts = 5
            }
        }
        var widthOfMediaStatusContainer = Math.round((contentSliderWidth * init_slider_width) / 100);

        $(".content-slider-media-status").width(widthOfMediaStatusContainer);


        //Full width containing invisible container
        $(".content-slider-media-container").width(numberOfChildren * widthOfMediaStatusContainer);

        if ((numberOfChildren - (100 / (100 / Math.round(contentSliderWidth / widthOfMediaStatusContainer)))) < (init_slider_id - 1)) {
            init_slider_id = ((numberOfChildren - (100 / (100 / Math.round(contentSliderWidth / widthOfMediaStatusContainer)))) + 1);
        }
        $(".content-slider-media-container").css("left", "-" + (((init_slider_id - 1) * widthOfMediaStatusContainer) - 0) + "px");
    };

    calculateSliderId = function(currentSlideId, numberOfItemsToSlide, numberOfElements, numberOfViewableElements, slideDirection) {

        if (slideDirection == "back") {
            if (currentSlideId > 1) {
                var new_slide_id = currentSlideId - visibleProducts;

                //Verhindern dass der Slider in den Minusbereich slided (1 = kleister Slide)
                return Math.max(new_slide_id, 1);
            }
        } else if (slideDirection == "next"){
            if (currentSlideId <= (numberOfElements - (numberOfViewableElements))) {
                var new_slide_id = currentSlideId + visibleProducts

                //Verhindern das slider ins Off slided (höchster Slide = Anzahl der Elmente minus Sichtbare Elemente)
                return Math.min(numberOfElements - (numberOfViewableElements), new_slide_id);
            }
        }
    }

    $(".content-slider-navigation-button").click(function () {
        var numberOfElementsToSlide = visibleProducts;
        var contentSliderWidth = $(".content-slider").width();
        var numberOfChildren = $(".content-slider-media-container").children().size();
        var widthOfMediaStatusContainer = Math.round((contentSliderWidth * init_slider_width) / 100);
        var numberOfViewableElements = 100 / (100 / Math.round(contentSliderWidth / widthOfMediaStatusContainer));

        init_slider_id = calculateSliderId(init_slider_id, numberOfElementsToSlide,numberOfChildren, numberOfViewableElements, $(this).attr("id"))


        $(".content-slider-media-container").animate({"left": "-" + ((init_slider_id - 1) * widthOfMediaStatusContainer) + "px"}, 500, function () {
        });
    })
});