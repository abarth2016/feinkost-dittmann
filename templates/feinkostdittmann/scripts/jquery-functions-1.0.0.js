jQuery(document).ready(function($) {
    var init_slideshow_id = 1;
    var init_slideshow_width = 0;
    var init_slideshow_height = 0;
    var init_slideshow_status_index = 0;
    var init_slideshow_activated;
    var init_slideshow_button_activated = true;

    $(".menu li").click(function()  {
         $(this).children("ul:first").toggleClass("mobile-nav-visible");

    });

    var positionSlideshow = function(){
        $(".header-slideshow-media-container").css("width", $(".header-slideshow").width() + "px");
        $(".header-slideshow-media").css("width", ($('.header-slideshow-media').children().size() * $(".header-slideshow-media-container").width()) + "px");
        if($("#header").css("position") === "fixed") {
            $(".header-slideshow").css("margin-top", ($("#header").height() - 1) + "px");
        } else {
            $(".header-slideshow").css("margin-top", (0 - 1) + "px");
        }
    }

    $(window).load(function() {
        positionSlideshow();
        for(var i = 1; i < $('.header-slideshow-media').children().size(); i++) {
            if(i < 2) {
                $(".header-slideshow-status").append("<div class='activated' slideshow-id='" + i + "'></div>");
            } else {
                $(".header-slideshow-status").append("<div class='' slideshow-id='" + i + "'></div>");
            }
        }

        /* After calculation right size, make image visible */
        placeSliderOverlays();

        $('.header-slideshow-media  img').css('visibility', 'visible');
        $('.header-slideshow-media-container-status-headline').css('visibility', 'visible');
        $('#header-preview img').css('visibility', 'visible');

        init_slideshow_interval();
        init_header_preview_function();
        init_slide_grid_pro();
        init_contact_form_function();
         /* init_slider_function(); */

        //Linking slider status buttons to slides
        $(".header-slideshow-status div").click(function(){
            $(".header-slideshow-media").stop();
            clearInterval(init_slideshow_activated);
            init_slideshow_interval();

            init_slideshow_id = $(this).attr('slideshow-id');
            init_slideshow_status_index = init_slideshow_id - 1 ;

            $(".header-slideshow-media").animate({"left": "-" + ((init_slideshow_id - 1) * $(".header-slideshow").width()) + "px"}, 1000, function() {   });
            $(".header-slideshow-status div").removeClass("activated");
            $(".header-slideshow-status div:nth-child(" + (init_slideshow_id ) + ")").addClass("activated");

        } );


    });

    $(window).resize(function() {

        positionSlideshow();

        clearInterval(init_slideshow_activated);
        $(".header-slideshow-media").stop();
        $(".header-slideshow-media").css("left", "-" + ((init_slideshow_id - 1) * $(".header-slideshow").width()) + "px");

        if(init_slideshow_id > ($('.header-slideshow-media').children().size() - 1)) {
            init_slideshow_id = 1;
            $(".header-slideshow-media").css("left", "0px");
        }

        init_slideshow_interval();
        init_header_preview_function();
        init_slide_grid_pro();

        /* init_slider_function();*/

        placeSliderOverlays();
    });
    var placeSliderOverlays = function(){
        if ($(".header-slideshow-media-container").length) {
            if ($(".header-slideshow-media-container").length) {
                //check if page is front page
                var headerHeight = $("#header").height();
                var positionOfGreenLine = $('.content-distance').position();
                var positionOfGreenLineTop = positionOfGreenLine.top;
                var heightOfSlideshow = $('.header-slideshow').height();
                var positionOfSearchInput = $('.header-slideshow-search').offset();
                var heightOfFontBox = $('.header-slideshow-media-container-status-headline').height();

                //position badge
                var heightOfBadgeBox = $('.header-slideshow-badge').height();
                var windowHeight = $(window).height();

                if ((heightOfSlideshow +headerHeight)> (windowHeight)) {
                    var positionFromBottom = heightOfSlideshow - windowHeight + headerHeight;
                }else{
                    var positionFromBottom = 5;
                }
                $(".header-slideshow-badge").css("bottom",positionFromBottom);

                //position toTopButton
                if ((heightOfSlideshow +headerHeight)> (windowHeight)) {
                    var positionToTopFromBottom = heightOfSlideshow - windowHeight + headerHeight+10;
                }else{
                    var positionToTopFromBottom = 20;
                }
                $(".header-slideshow-badge+.bt-scroll-top").css("bottom",positionToTopFromBottom+"px");

                //position FontBox
                if ((heightOfSlideshow +headerHeight)> (windowHeight)) {
                    var positionFontBoxFromBottom = heightOfSlideshow - windowHeight + headerHeight ;
                }else{
                    var positionFontBoxFromBottom = 5;
                }
                var positionSearchBox = ((heightOfSlideshow+ headerHeight) -(positionOfSearchInput.top+heightOfFontBox) - $('.header-slideshow-search').height());
                
                $(".header-slideshow-media-container-status").css("bottom",Math.min(positionFontBoxFromBottom, positionSearchBox)+"px");
            }
        }
    }
    /* HEADER MOBILE NAVIGATION FUNCTIONS */

    var init_mobile_navigation_activated = false;

    $(".header-navigation-mobile").click(function() {
        if($(".header-navigation.font-white ul:first").css("display") == "none") {
            //mobile Navigation aufgeklappt
            init_mobile_navigation_activated = false;

        } else {
            //mobile Navigation zugeklappt
            init_mobile_navigation_activated = true;
        }

        if(init_mobile_navigation_activated == true) {
            init_mobile_navigation_activated = false;
            $(".header-navigation.font-white ul:first").css({"display": ""});
        } else if(init_mobile_navigation_activated == false) {
            init_mobile_navigation_activated = true;

            $(".header-navigation.font-white ul:first").css({"display": "block"});
        }

        if($("#header").css("position") === "fixed") {
            $(".header-slideshow").css("margin-top", ($("#header").height() - 1) + "px");
        } else {
            $(".header-slideshow").css("margin-top", (0 - 1) + "px");
        }
    });

    /* HEADER SLIDESHOW FUNCTIONS */

    init_slideshow_interval = function() {

        clearInterval(init_slideshow_activated);
        init_slideshow_activated = setInterval(function() {
            init_slideshow_button_activated = false;
            init_slideshow_id++;

            $(".header-slideshow-media").animate({"left": "-" + ((init_slideshow_id - 1) * $(".header-slideshow").width()) + "px"}, 1000, function() {
                if(init_slideshow_id > ($('.header-slideshow-media').children().size() - 1)) {
                    init_slideshow_id = 1;
                    $(".header-slideshow-media").css("left", "0px");
                }
                init_slideshow_button_activated = true;
            });

            init_slideshow_status_index++;
            if(init_slideshow_status_index > ($('.header-slideshow-media').children().size() - 2)) {
                init_slideshow_status_index = 0;
            }
            $(".header-slideshow-status div").removeClass("activated");
            $(".header-slideshow-status div:nth-child(" + (init_slideshow_status_index + 1) + ")").addClass("activated");
        }, 6500);

    };

    $(".bt-scroll-top").on('click', (
        function(e){
            var element = $(this).attr('href');
            var headerHeight = $('#header').height();
            var targetTop = $(element).offset().top;
            var newPosition = targetTop - headerHeight;

            $('html, body').animate({ scrollTop: newPosition+"px"}, 'slow');
    
            e.preventDefault();
        }));


    $(".bt-scroll-bottom").on('click', (
        function(e){
            var headerHeight = $('#header').height();
            var targetTop = $('.header-slideshow-media').offset().top;
            var newPosition = targetTop - headerHeight;
            $('html, body').animate({ scrollTop: newPosition}, 'slow');
    
            e.preventDefault();
        }));


    $(".header-slideshow-button").click(function() {

        if(init_slideshow_button_activated == true) {
            init_slideshow_button_activated = false;
            clearInterval(init_slideshow_activated);
            init_slideshow_id++;

            $(".header-slideshow-media").animate({"left": "-" + ((init_slideshow_id - 1) * $(".header-slideshow").width()) + "px"}, 1000, function() {
                if(init_slideshow_id > ($('.header-slideshow-media').children().size() - 1)) {
                    init_slideshow_id = 1;
                    $(".header-slideshow-media").css("left", "0px");
                }

                init_slideshow_interval();
                init_slideshow_button_activated = true;
            });

            init_slideshow_status_index++;
            if(init_slideshow_status_index > ($('.header-slideshow-media').children().size() - 2)) {
                init_slideshow_status_index = 0;
            }

            $(".header-slideshow-status div").removeClass("activated");
            $(".header-slideshow-status div:nth-child(" + (init_slideshow_status_index + 1) + ")").addClass("activated");
        }

    });
    
    /* HEADER PREVIEW FUNCTIONS */

    init_header_preview_function = function() {
        $("#header-preview").css("height", ((($(window).width() * 28) / 100)) + "px");
        if($("#header-preview").height() >= 460) {
            $("#header-preview").css("height", (460 + 0) + "px");
        } else if($("#header-preview").height() <= 180) {
            $("#header-preview").css("height", (180 + 0) + "px");
        }

        var init_preview_width = $("#header-preview img:first").width() / $("#header-preview img:first").height();
        var init_preview_height = $("#header-preview img:first").height() / $("#header-preview img:first").width();

        if(($("#header-preview").height() / $("#header-preview").width()) < init_preview_height) {
            $("#header-preview img:first").css("width", $("#header-preview").width());
            $("#header-preview img:first").css("height", init_preview_height * $("#header-preview").width());
        } else {
            $("#header-preview img:first").css("height", $("#header-preview").height());
            $("#header-preview img:first").css("width", init_preview_width * $("#header-preview").height());
        }

        $("#header-preview img:first").css("margin-top", ($("#header-preview").height() - $("#header-preview img:first").height()) / 2);
        $("#header-preview img:first").css("margin-left", ($("#header-preview").width() - $("#header-preview img:first").width()) / 2);
        $('#header-preview img').css('visibility', 'visible');
    };

    /* SLIDE GRID PRO FUNCTIONS */

    init_slide_grid_pro = function() {
        $(".content-products").SlideGridPro({
            setColumns: "4",
            setMarginX: "36",
            setMarginY: "60",
            setElement: "div"
        });

        $(".content-trademarks").SlideGridPro({
            setColumns: "4",
            setMarginX: "36",
            setMarginY: "60",
            setElement: "div"
        });
        $(".content-searchresults").SlideGridPro({
            setColumns: "4",
            setMarginX: "36",
            setMarginY: "60",
            setElement: "div"
        });

        $("#content").SlideGridPro({
            setColumns: "auto",
            setMarginX: "0",
            setMarginY: "12",
            setElement: "div"
        });

  	if($(".content-products-container").length || $(".content-productgroups-container").length || $(".content-searchresults-container").length ){
        $(".content-products-container").css('visibility', 'visible');
        $(".content-productgroups-container").css('visibility', 'visible');
        $(".content-searchresults-container").css('visibility', 'visible');

    }

    if($(".content-products").length){
        $(".content-products").css('background-image', 'none');
    }
    };
    /* TRADEMARKS FUNCTIONS */

    $(".content-trademarks-container").mouseover(function() {
        $(this).find(".content-trademarks-status").stop();
        $(this).find(".content-trademarks-status").animate({"opacity": 1}, 300, function() {
        });
    });

    $(".content-trademarks-container").mouseout(function() {
        $(this).find(".content-trademarks-status").stop();
        $(this).find(".content-trademarks-status").animate({"opacity": 0}, 300, function() {
        });
    });
    /* INGREDIENTS FUNCTIONS */

    function calculateDecimal(fraction) {
        if(fraction.indexOf(" ") !== -1) {
            var decimal = fraction.split(" ");
            var digit = decimal[1].split("/");
            var result = parseInt(decimal[0], 10) + (parseInt(digit[0], 10) / parseInt(digit[1], 10));
        } else {
            if(fraction.indexOf("/") !== -1) {
                var digit = fraction.split("/");
                var result = (parseInt(digit[0], 10) / parseInt(digit[1], 10));
            } else {
                var result = fraction;
            }
        }
        return result;
    }

    function calculateFraction(decimal) {
        fraction = String(decimal).split(".")[0];
        decimal = parseFloat("." + String(decimal).split(".")[1]);
        digit = "1";
        for(i = 0; i < String(decimal).length - 2; i++) {
            digit += "0";
        }
        decimal = parseInt(decimal * digit);
        digit = parseInt(digit);
        for(i = 2; i < decimal + 1; i++) {
            if(decimal % i == 0 && digit % i == 0) {
                decimal = decimal / i;
                digit = digit / i;
                i = 2;
            }
        }
        if(!isNaN(decimal) == true && !isNaN(digit) == true) {
            var result = ((fraction == 0) ? "" : fraction + " ") + decimal + "/" + digit;
        } else {
            var result = (fraction == 0) ? "" : fraction + "";
        }
        return result;
    }

    var values = [];
    $("#multiplicator").val("1");
    $("#content span").each(function() {
        if($(this).html() != "") {
            values.push($(this).html());
        }
    });

    $("#content-ingredients-button-convert").click(function() {
        if($("#number span input").val().match(/^[0-9]+$/)) {
            var index = 0;
            var multiplicator = $("#number span input").val();
            if(multiplicator <= 0) {
                multiplicator = 1;
                $("#multiplicator").val("1");
            }
            if(multiplicator >= 120) {
                multiplicator = 120;
                $("#multiplicator").val("120");
            }
            $("#content-ingredients-notification").css({"display" : ""});
            if(multiplicator >= 50) {
                /* $("#content-ingredients-notification").css({"display" : "block"}); */
            }
            multiplicator = multiplicator / 4;
            $("#content span").each(function() {

                if($(this).html() != "") {
                    var decimal = ((calculateDecimal(values[index])) * multiplicator);
                    var result = calculateFraction(decimal);
                    $(this).html(result);
                    index++;
                }
            });
            /* change jsonString for Shop */
            changeQuantityOfIngredients(multiplicator);
        } else {
            var index = 0;
            $("#multiplicator").val("1");
            $("#content-ingredients-notification").css({"display" : ""});
            $("#content span").each(function() {
                if($(this).html() != "") {
                    var decimal = (calculateDecimal(values[index]) * 1);
                    var result = calculateFraction(decimal);
                    $(this).html(result);
                    index++;
                }
            });
        }
        init_slide_grid_pro();
    });
    
    /* CONTACT FORM FUNCTIONS */
    init_contact_form_function = function() {
        $("#message").val("");
    };

    $("#subject").on('change', function() {
        if($(this).val() == "Reklamation" || $(this).val() == "customer complaint") {
            $(".content-contact-form .ean").css({"display": "block"});
            $(".content-contact-form .mhd").css({"display": "block"});
        } else {
            $(".content-contact-form .ean").css({"display": "none"});
            $(".content-contact-form .mhd").css({"display": "none"});
        }
    });
});
/* Change quantities that are given to the shop via "in den Warenkorb legen" in #jsonString
* called in recipe detail */
changeQuantityOfIngredients = function(multiplicator) {
    var arrayWithQuantities =  $.parseJSON(($('#jsonString').val()));
    $.each( arrayWithQuantities, function( index, value ) {
        value['qty'] =  value['qty'] * multiplicator;
    });
    $('#jsonString').val(JSON.stringify(arrayWithQuantities));
}