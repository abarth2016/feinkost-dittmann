$(window).load(function()  {
    $("#content-gewinnspiel-button").click(function () {
        var regular_expression = /^([\w-\.]+@(?!rmp.com)([\w-]+\.)+[\w-]{2,4})?$/;

        $(".content-contact-form-notification").css({"display": ""});
        
        if ($("#mail").val().toLowerCase() != "" && regular_expression.test($("#mail").val().toLowerCase())) {
            $.ajax({
                type: "POST",
                url: location.protocol + "//" + location.host + "/feinkost-dittmann/templates/feinkostdittmann/functions/mail.php",
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: new FormData($("#form")[0]),
                success: function (data) {
                    if (data == "true") {
                        $(".content-contact-form-notification").css({
                            "display": "block",
                            "background-color": "#008846"
                        });
                    }
                    else {
                        $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                        $(".content-contact-form-notification").css({
                            "display": "block",
                            "background-color": "#CC0033"
                        });
                    }
                },
                beforeSend: function () {
                    $(".content-contact-form-notification").css({"display": ""});
                }
            });
        }
        else {
            $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
            $(".content-contact-form-notification").css({"display": "block", "background-color": "#CC0033"});
        }
    });
});
