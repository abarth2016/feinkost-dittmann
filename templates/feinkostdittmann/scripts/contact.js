jQuery(document).ready(function ($) {
    var dataOkay=0;

    function newRechenaufgabe(){

        $.ajax({
            type: "POST",
            async: false,
            url: location.protocol + "//" + location.host + "/feinkost-dittmann/templates/feinkostdittmann/functions/getMission.php",
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: new FormData($("#form")[0]),

            success: function (data) {
                if(data !=="false" && data !== "true"){
                    $("#rechenaufgabe").html(data);
                }else{
                    if (data == "true"){
                        dataOkay=1;
                    }else{
                    }
                }
            }
        });
        return true;
    }
    newRechenaufgabe();

    $("#content-contact-form-button").click(function () {
        var regular_expression = /^([\w-\.]+@(?!rmp.com)([\w-]+\.)+[\w-]{2,6})?$/;
        $(".content-contact-form-notification").css({"display": ""});
        newRechenaufgabe();
        if ($('#handelspartner').length == 0) {
            if ($("#name").val().toLowerCase() != "" && $("#surname").val().toLowerCase() != "" && $("#message").val() != "") {
                if ($("#mail").val().toLowerCase() != "" && regular_expression.test($("#mail").val().toLowerCase())) {
                    if(newRechenaufgabe() == true) {
                        if ($("select[name=subject]").val().toLowerCase() != "reklamation" || $("select[name=subject]").val().toLowerCase() == "reklamation" && $("#ean").val().toLowerCase() != "" && $("#mhd").val().toLowerCase() != "") {
                            $.ajax({
                                type: "POST",
                                url: location.protocol + "//" + location.host + "/feinkost-dittmann/templates/feinkostdittmann/functions/mail.php",
                                dataType: 'text',
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: new FormData($("#form")[0]),
                                success: function (data) {
                                    if (data == "true") {
                                        $("#salutation").find("option:eq(0)").prop("selected", true);
                                        $("#name").val("");
                                        $("#surname").val("");
                                        $("#company").val("");
                                        $("#address").val("");
                                        $("#zip").val("");
                                        $("#location").val("");
                                        $("#country").val("");
                                        $("#phone").val("");
                                        $("#mail").val("");
                                        $("#subject").find("option:eq(0)").prop("selected", true);
                                        $("#ean").val("");
                                        $("#mhd").val("");
                                        $("#message").val("");
                                        $("#attachment").val("");
                                        $("#code").val("");
                                        $(".content-contact-form .ean").css({"display": "none"});
                                        $(".content-contact-form .mhd").css({"display": "none"});
                                        $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-true").html() + "</span>");
                                        $(".content-contact-form-notification").css({
                                            "display": "block",
                                            "background-color": "#008846"
                                        });
                                        $('#code').html();
                                    } else if (data == "false") {
                                        $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                                        $(".content-contact-form-notification").css({
                                            "display": "block",
                                            "background-color": "#CC0033"
                                        });
                                        $('#code').html();
                                        newRechenaufgabe();

                                    } else {
                                        $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                                        $(".content-contact-form-notification").css({
                                            "display": "block",
                                            "background-color": "#CC0033"
                                        });
                                        $('#code').html();
                                        newRechenaufgabe();
                                    }
                                },
                                beforeSend: function () {
                                    $(".content-contact-form-notification").css({"display": ""});
                                }
                            });

                        } else {
                            $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                            $(".content-contact-form-notification").css({
                                "display": "block",
                                "background-color": "#CC0033"
                            });
                        }
                    }else{
                        $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                        $(".content-contact-form-notification").css({"display": "block", "background-color": "#CC0033"});
                    }
                } else {
                    $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                    $(".content-contact-form-notification").css({"display": "block", "background-color": "#CC0033"});
                }
            } else {
                $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                $(".content-contact-form-notification").css({"display": "block", "background-color": "#CC0033"});
            }
        } else if ($('#handelspartner').length > 0) {
            if ($("#name").val().toLowerCase() != "" && $("#surname").val().toLowerCase() != "" && $("#message").val() != "") {
                if ($("#company").val().toLowerCase() != "" && $("#message").val() != "") {

                    if ($("#mail").val().toLowerCase() != "" && regular_expression.test($("#mail").val().toLowerCase())) {

                        if ($("#subject").val().toLowerCase() != "") {
                            $.ajax({
                                type: "POST",
                                url: location.protocol + "//" + location.host + "/feinkost-dittmann/templates/feinkostdittmann/functions/mail.php",
                                dataType: 'text',
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: new FormData($("#form")[0]),
                                /*
                                 data: {
                                 salutation: $("select[name=salutation]").val(),
                                 name: $("#name").val(),
                                 surname: $("#surname").val(),
                                 company: $("#company").val(),
                                 address: $("#address").val(),
                                 zip: $("#zip").val(),
                                 location: $("#location").val(),
                                 country: $("#country").val(),
                                 phone: $("#phone").val(),
                                 mail: $("#mail").val(),
                                 subject: $("select[name=subject]").val(),
                                 ean: $("#ean").val(),
                                 mhd: $("#mhd").val(),
                                 attachment: $("#attachment").prop("files")[0],
                                 message: $("#message").val()
                                 },
                                 */
                                success: function (data) {
                                    if (data == "true") {
                                        $("#name").val("");
                                        $("#surname").val("");
                                        $("#company").val("");
                                        $("#mail").val("");
                                        $("#subject").val("");
                                        $("#message").val("");
                                        $("#attachment").val("");
                                        $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-true").html() + "</span>");
                                        $(".content-contact-form-notification").css({
                                            "display": "block",
                                            "background-color": "#008846"
                                        });
                                        $('#code').html();
                                        dataOkay = 0;
                                    } else if (data == "false") {

                                        $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                                        $(".content-contact-form-notification").css({
                                            "display": "block",
                                            "background-color": "#CC0033"
                                        });
                                        $('#code').html();
                                        newRechenaufgabe();
                                        dataOkay = 0;
                                    } else {

                                        $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                                        $(".content-contact-form-notification").css({
                                            "display": "block",
                                            "background-color": "#CC0033"
                                        });
                                        $('#code').html();
                                        newRechenaufgabe();
                                        dataOkay = 0;
                                    }
                                },
                                beforeSend: function () {
                                    $(".content-contact-form-notification").css({"display": ""});
                                }
                            });
                        } else {
                            $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                            $(".content-contact-form-notification").css({
                                "display": "block",
                                "background-color": "#CC0033"
                            });
                        }
                    } else {
                        $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                        $(".content-contact-form-notification").css({
                            "display": "block",
                            "background-color": "#CC0033"
                        });
                    }
                } else {
                    $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
                    $(".content-contact-form-notification").css({"display": "block", "background-color": "#CC0033"});
                }
            } else {
            $(".content-contact-form-notification").html("<span>" + $(".content-contact-form-notification-false").html() + "</span>");
            $(".content-contact-form-notification").css({"display": "block", "background-color": "#CC0033"});
        }}
    });
});
