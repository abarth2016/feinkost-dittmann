<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php echo JHTML::_('content.prepare', '{loadposition header}'); ?>

<div id="header-preview"><img src="images/header-preview/header-preview-contact-01.jpg" alt="" /></div>

<?php echo JHTML::_('content.prepare', '{loadposition content-distance}'); ?>

<!-- CONTENT -->
<div class="content-searchresults">

<?php foreach ($this->results as $result) : ?>

		<div class=" content-searchresults-container">
			<a href="<?php echo JRoute::_($result->href); ?>"<?php if($result->browsernav == 1) : ?> target="_blank"<?php endif; ?>>
					<div class="content-searchresults-status-preview<?php echo isset($result->resulttype)?" content-result-".strtolower($result->resulttype):"";?>">
					<?php

					if(!empty($result->filename)) {
						if (file_exists($result->filename)) {
							echo "<img style\"z-index:2;\" src=\"" . $result->filename . "\" alt=\"" . htmlentities($result->title) . "\" />";
						} else {
							echo "<img style\"z-index:2;\" src=\"images/content-products-category/no_image.png\" alt=\"" . $result->filename . ".png\" />";
						}
						}
					?>

					</div>
				<img src="images/content-products-category/background.jpg" alt="" />

				<div class="content-searchresults-status-preview container-hover">
					<div class="content-searchresults-hover-text">
					<?php  echo $this->escape($result->title);
					if(isset($result->markenname)){
					?><br>

					<span class="csr-left">Marke:</span><span class="csr-right"><?php  echo $this->escape($result->markenname); ?></span>
					<span class="csr-left">Artikel-Nr.:</span><span class="csr-right"><?php  echo $this->escape($result->artikel_nr); ?></span>
					<span class="csr-left">EAN:</span><span class="csr-right"><?php  echo $this->escape($result->ean); ?></span>
					<?php } ?>
					</div>

				</div>
				</a>
			</div>


<?php endforeach; ?>
</div>


<div class="custom-pagination">

	<?php echo $this->pagination->getPagesLinks(); ?>

</div>

<?php echo JHTML::_('content.prepare', '{loadposition content-distance}'); ?>

<?php echo JHTML::_('content.prepare', '{loadposition footer}'); ?>
