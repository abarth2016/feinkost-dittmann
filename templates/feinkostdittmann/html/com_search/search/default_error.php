<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->error) : ?>

	<?php echo JHTML::_('content.prepare', '{loadposition header}'); ?>

	<div id="header-preview"><img src="images/header-preview/header-preview-contact-01.jpg" alt="" /></div>

	<?php echo JHTML::_('content.prepare', '{loadposition content-distance}'); ?>

	<div class="content-articles">

	<div class="content-articles-headline">

		Suchergebnis

	</div>

	<div class="content-articles-description">

			<?php echo $this->escape($this->error); ?>

	</div>

</div>

<?php endif; ?>

<?php echo JHTML::_('content.prepare', '{loadposition content-distance}'); ?>

<?php echo JHTML::_('content.prepare', '{loadposition footer}'); ?>
