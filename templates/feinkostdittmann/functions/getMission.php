<?php
session_start();

function newSession()
{
    $_SESSION['zahl1'] = mt_rand(1, 100);
    $_SESSION['zahl2'] = mt_rand(1, 100);
    $_SESSION['ergebnis'] = $_SESSION['zahl1'] + $_SESSION['zahl2'];
}

if(!isset($_SESSION['ergebnis']))
    newSession();

if(isset($_POST['code']) && $_POST['code'] !="")
{
    //echo "code:".$_POST['code']."=".$_SESSION['ergebnis'];
    if((int)$_POST['code'] != $_SESSION['ergebnis'] ||
        strpos($_POST['code'], '.') !== false ||
        !is_numeric($_POST['code']) || is_array($_POST['code']))
    {
        $_SESSION["emailsend_allowance"] = 0;
        echo 'false';
    } else {
        echo 'true';
        $_SESSION["emailsend_allowance"] = 1;
        exit;
    }
}else{
    $rechenaufgabe = $_SESSION['zahl1'] . ' + ' . $_SESSION['zahl2'] . ' = ';
    echo $rechenaufgabe;
}
?>