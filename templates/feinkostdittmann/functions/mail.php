<?php
session_start();
function mailCustomer($mail, $salutation, $name)
{

    $headers = "From: Feinkost Dittmann <info@feinkost-dittmann.de>" . "\r\n";

    $headers .= "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8" . "\r\n";

    $message = "Sehr geehrte(r) " . $salutation . "  " . $name . ",<br /><br>";
    $message .= "vielen Dank für Ihre Nachricht.<br /><br />
                Gerne werden wir uns um Ihr Anliegen kümmern, dies kann ein paar Tage in Anspruch nehmen. Wir werden uns in Kürze mit Ihnen in Verbindung setzen.<br /><br />
                Mit freundlichen Grüßen<br /><br />
                Feinkost Dittmann<br>
                Reichold Feinkost GmbH<br>
                Kundenservice<br /><br />
                August-Horch-Straße 4-8<br>
                D-65582 Diez<br /><br />
                Reg.-Gericht Montabaur HRB 5801<br>
                Geschäftsführer: Timm J. Reichold, Thorsten Reichold Bbn.-Betriebsnummer 4002239 USt.-IdNr. DE812416609 EU Zulassungsnummer DE- HE 10800-EG";

    $subject = "Ihre Nachricht an Feinkost Dittmann";

    mail($mail, $subject, $message, $headers);
}

$to = "web@rautenberg.media";


if (isset($_POST["salutation"])) {
    if (strtolower($_POST["salutation"]) == "herr") {
        $subject = "" . $_POST["subject"] . " von: " . $_POST["salutation"] . "(n) " . $_POST["name"] . " " . $_POST["surname"] . "";
    } else {
        $subject = "" . $_POST["subject"] . " von: " . $_POST["salutation"] . " " . $_POST["name"] . " " . $_POST["surname"] . "";
    }

    $headers = "From: Feinkost Dittmann <info@feinkost-dittmann.de>" . "\r\n";

    $headers .= "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8" . "\r\n";
    $message = "<html><body>";

    $message .= "Sehr geehrte Damen und Herren,<br /><br />";
    $message .= "Sie haben eine neue Nachricht erhalten:<br /><br />";

    if ($_POST["company"] != "") {
        $message .= $_POST["company"] . "<br />";
    }

    if (strtolower($_POST["salutation"]) == "herr") {
        $message .= $_POST["salutation"] . " " . $_POST["name"] . " " . $_POST["surname"] . "<br />";
    } else {
        $message .= $_POST["salutation"] . " " . $_POST["name"] . " " . $_POST["surname"] . "<br />";
    }

    if ($_POST["address"] != "") {
        $message .= $_POST["address"] . "<br />";
    }

    if ($_POST["zip"] != "" && $_POST["location"] != "") {
        $message .= $_POST["zip"] . " " . $_POST["location"] . "<br />";
    }

    if ($_POST["country"] != "") {
        $message .= $_POST["country"] . "<br /><br />";
    }

    if ($_POST["phone"] != "") {
        $message .= "Telefon: " . $_POST["phone"] . "<br /><br />";
    }

    if ($_POST["ean"] != "") {
        $message .= "EAN: " . $_POST["ean"] . "<br /><br />";
    }

    if ($_POST["mhd"] != "") {
        $message .= "MHD: " . $_POST["mhd"] . "<br /><br />";
    }
    $message .= "E-Mail: " . $_POST["mail"] . "<br /><br />";
    $message .= "Nachricht: <br />";
    $message .= $_POST["message"];

    if ($_FILES["attachment"]["name"]) {
        $info = pathinfo($_FILES["attachment"]["name"]);
        $extension = $info["extension"];
        $extension_array = array("jpg", "JPG", "jpeg", "JPEG", "png", "PNG", "pdf", "PDF");
        $target = "/kunden/275773_65232/webseiten/feinkost-dittmann/uploads/" . $_FILES["attachment"]["name"];

        if (in_array($extension, $extension_array) && round($_FILES["attachment"]["size"] / 1024) <= 3072) {

            move_uploaded_file($_FILES["attachment"]["tmp_name"], $target);

            $message .= "<br /><br />";
            $message .= "Download Link: ";
            $message .= "<br />";
            $message .= "<a href='http://" . $_SERVER['HTTP_HOST'] . "/feinkost-dittmann/uploads/" . $_FILES["attachment"]["name"] . "' target='_blank' download='" . $_FILES["attachment"]["name"] . "'>" . $_SERVER['HTTP_HOST'] . "/feinkost-dittmann/uploads/" . $_FILES["attachment"]["name"] . "</a>";

            $upload = true;

        } else {

            $upload = false;

        }

    }
} else {
    //Handelspartner
    $subject = "" . isset($_POST["subject"]) ? $_POST["subject"] : '' . " von: " . (isset($_POST["company"]) ? $_POST["company"] : '') . "";


    $headers = "From: Feinkost Dittmann <info@feinkost-dittmann.de>" . "\r\n";

    $headers .= "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8" . "\r\n";
    $message = "<html><body>";

    $message .= "Sehr geehrte Damen und Herren,<br /><br />";
    $message .= "Sie haben eine neue Nachricht erhalten:<br /><br />";

    if (isset($_POST["company"]) && $_POST["company"] != "") {
        $message .= $_POST["company"] . "<br />";
    }

    $message .= "E-Mail: " . (isset($_POST["mail"]) ? $_POST["mail"] : '') . "<br /><br />";
    $message .= "Nachricht: <br />";
    $message .= isset($_POST["message"]) ? $_POST["message"] : '';

}

$message .= "</body></html>";
if (isset($_SESSION["emailsend_allowance"])) {
    if (isset($upload)) {

        if ($upload === true) {

            if (mail($to, $subject, $message, $headers)) {

                mailCustomer($_POST["mail"], $_POST["salutation"], $_POST["name"] . " " . $_POST["surname"]);

                echo "true";
                session_destroy();

            } else {
                echo "Mail wurde nicht versendet";

                echo "false";
                session_destroy();

            }

        } else {
            echo "false";
            session_destroy();
        }

    } else {

        if (mail($to, $subject, $message, $headers)) {

            mailCustomer($_POST["mail"], $_POST["salutation"], $_POST["name"] . " " . $_POST["surname"]);
            echo "true";
            session_destroy();
        } else {
            echo "false";
        }
    }

}else{
    echo "Keine Erlaubnis";
    session_destroy();
}

?>