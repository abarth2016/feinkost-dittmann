<?php
//First start with information about the Plugin and yourself. For example:
/**
 * @package     Joomla.Plugin
 * @subpackage  Search.products
 *
 * @copyright   Copyright
 * @license     License, for example GNU/GPL
 */

//To prevent accessing the document directly, enter this code:
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Require the component's router file (Replace 'search-custom' with the component your providing the search for
//require_once JPATH_SITE .  '/components/products/helpers/route.php';

/**
 * All functions need to get wrapped in a class
 *
 * The class name should start with 'PlgSearch' followed by the name of the plugin. Joomla calls the class based on the name of the plugin, so it is very important that they match
 */
class PlgSearchRecipes extends JPlugin
{
    /**
     * Constructor
     *
     * @access      protected
     * @param       object  $subject The object to observe
     * @param       array   $config  An array that holds the plugin configuration
     * @since       1.6
     */
    public function __construct(& $subject, $config)
    {
        parent::__construct($subject, $config);
        $this->loadLanguage();
    }

    // Define a function to return an array of search areas. Replace 'nameofplugin' with the name of your plugin.
    // Note the value of the array key is normally a language string
    public function onContentSearchAreas()
    {
        static $areas = array(
            'recipes' => 'Rezepte'
        );
        return $areas;
    }

    // The real function has to be created. The database connection should be made.
    // The function will be closed with an } at the end of the file.
    /**
     * The sql must return the following fields that are used in a common display
     * routine: href, title, section, created, text, browsernav
     *
     * @param string Target search string
     * @param string mathcing option, exact|any|all
     * @param string ordering option, newest|oldest|popular|alpha|category
     * @param mixed An array if the search it to be restricted to areas, null if search all
     */
    function onContentSearch( $text, $phrase='', $ordering='', $areas=null )
    {
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();
        $user	= JFactory::getUser();
        $groups	= implode(',', $user->getAuthorisedViewLevels());

        // If the array is not correct, return it:
        if (is_array( $areas )) {
            if (!array_intersect( $areas, array_keys( $this->onContentSearchAreas() ) )) {
                return array();
            }
        }

		$sContent = $this->params->get('search_content', 1);
		$sArchived = $this->params->get('search_archived', 1);
		$limit = $this->params->def('search_limit', 50);
		$state = array();


        // Use the PHP function trim to delete spaces in front of or at the back of the searching terms
        $text = trim( $text );

        // Return Array when nothing was filled in.
        if ($text == '') {
            return array();
        }

        // After this, you have to add the database part. This will be the most difficult part, because this changes per situation.
        // In the coding examples later on you will find some of the examples used by Joomla! 3.1 core Search Plugins.
        //It will look something like this.
        $wheres = array();
        switch ($phrase) {

            // Search exact
            case 'exact':
                $text		= $db->quote( '%'.$db->escape( $text, true ).'%', false );
                $wheres2 	= array();
                $wheres2[] 	= 'LOWER(a.bezeichnung) LIKE '.$text;
                $where 		= '(' . implode( ') OR (', $wheres2 ) . ')';
                break;

            // Search all or any
            case 'all':
            case 'any':

                // Set default
            default:
                $words 	= explode( ' ', $text );
                $wheres = array();
                foreach ($words as $word)
                {
                    $word		= $db->Quote( '%'.$db->escape( $word, true ).'%', false );
                    $wheres2 	= array();
                    $wheres2[] 	= 'LOWER(a.bezeichnung) LIKE '.$word;
                    $wheres2[] 	= 'LOWER(a.zubereitung) LIKE '.$word;
                    $wheres[] 	= implode( ' OR ', $wheres2 );
                }
                $where = '(' . implode( ($phrase == 'all' ? ') AND (' : ') OR ('), $wheres ) . ')';
                break;
        }

        // Ordering of the results
        switch ( $ordering ) {

            //Alphabetic, ascending
            case 'alpha':
                $order = 'a.name ASC';
                break;

            // Oldest first
            case 'oldest':

                // Popular first
            case 'popular':

                // Newest first
            case 'newest':

                // Default setting: alphabetic, ascending
            default:
                $order = 'a.bezeichnung ASC';
        }

        // Replace nameofplugin
        $section = JText::_( 'Rezepte' );
        $query	= $db->getQuery(true);



        $query->select('a.bezeichnung AS title, \'\' AS created, a.zubereitung AS text, a.id as rezeptid')
            ->select($query->concatenate(array($db->quote($section), 'bezeichnung'), " / ") . ' AS section')
            ->select('\'1\' AS browsernav')
            ->from('recipes AS a')
            ->where('(' . $where . ')')
            ->order($order);

        // Set query
        $db->setQuery( $query, 0, $limit );
        $rows = $db->loadObjectList();

        // The 'output' of the displayed link. Again a demonstration from the newsfeed search plugin
        foreach($rows as $key => $row) {
            $string = mb_strtolower(html_entity_decode($rows[$key]->title));
            $string = preg_replace('/\x{00e4}/u', "ae", $string);
            $string = preg_replace('/\x{00fc}/u', "ue", $string);
            $string = preg_replace('/\x{00f6}/u', "oe", $string);
            $string = preg_replace('/\x{00df}/u', "ss", $string);
            $string = preg_replace('/\r\n/', "_", $string);
            $string = preg_replace('/\n/', "_", $string);
            $string = preg_replace('/\r/', "_", $string);
            $string = preg_replace('/\\015\\012|\\015|\\012/', "", $string);
            $string = preg_replace("/-/", "_", $string);
            $string = preg_replace('/\x{0020}/u', "_", $string);
            $string = preg_replace("/[^a-z0-9_]/", "", $string);
            $createdFileName = $string . "_" . str_pad($rows[$key]->rezeptid, 3, 0);
            $filename = 'images/rezeptbilder/618x390/'.$createdFileName.".jpg";
            $rows[$key]->filename = $filename;
            $rows[$key]->resulttype = "RECIPE";
            $rows[$key]->title = html_entity_decode($rows[$key]->title );
            $rows[$key]->href = 'index.php?option=com_content&view=article&id=57&recipeId='.$rows[$key]->rezeptid;
        }

        //Return the search results in an array
        return $rows;
    }
}